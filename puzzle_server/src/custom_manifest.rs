//! Alternative to a full-fledged IIIF manifest
//!
//! This module provides a simpler [`Manifest`] alternative to a full-fledged IIIF Manifest in
//! order to facilitate the definition of metadata on images.

use std::collections::HashMap;

use serde::{Deserialize, Serialize};

use crate::iiif::v3::{Homepage, MayBeI18NString, SeeAlso, StructuredMetadata};

/// Container for a homepage which could be a simple string (i.e. uri) or a full-blown [`Homepage`]
/// object
#[derive(Deserialize, Serialize, Clone, Debug)]
#[serde(untagged)]
pub enum MayBeHomepage {
    Uri(String),
    Homepage(Homepage),
}

/// Container for a see also notice which could be a simple string (i.e. uri) or a full-blown
/// [`SeeAlso`] object
#[derive(Deserialize, Serialize, Clone, Debug)]
#[serde(untagged)]
pub enum MayBeSeeAlso {
    Uri(String),
    SeeAlso(SeeAlso),
}

#[derive(Deserialize, Serialize, Clone, Debug)]
#[serde(untagged)]
/// Container for metadata which can be multilingual.
pub enum MayBeStructuredMetadata {
    /// Simple key value metadata
    SimpleMap(HashMap<String, String>),
    /// Metadata where the key and the value is multilingual
    StructuredMetadata(Vec<StructuredMetadata>),
}

/// A custom manifest describing an image.
#[derive(Deserialize, Debug, Serialize, Clone)]
#[serde(rename = "camelCase")]
pub struct Manifest {
    /// List of image uris
    pub uris: Vec<String>,
    /// Manifest's title
    pub label: MayBeI18NString,
    /// An optional content summary
    pub summary: Option<MayBeI18NString>,
    /// An optional declaration of usage rights
    pub rights: Option<String>,
    /// Resource owner
    pub provider: Option<Vec<MayBeI18NString>>,
    /// Web page that is about the object represented by the resource
    pub homepage: Option<Vec<MayBeHomepage>>,
    /// Optional see also resource
    pub see_also: Option<MayBeSeeAlso>,
    /// Region of image which should be imported (optional). See [IIIF Image API documentation](https://iiif.io/api/image/3.0/#41-region) for allowed syntax
    pub region: Option<String>,
    /// Optional rotation of image. See [IIIF Image API documentation](https://iiif.io/api/image/3.0/#43-rotation) for allowed syntax
    pub rotation: Option<String>,
    /// Optional mimetype
    pub format: Option<String>,
    /// Additional metadata
    pub metadata: Option<MayBeStructuredMetadata>,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_simple_manifest() {
        let manifest: Result<Manifest, serde_json::Error> = serde_json::from_str(
            r#"{
                "uris": ["file:///home/seb/night.jpg"],
                "label": "Shimla night"
            }"#,
        );
        assert!(manifest.is_ok());
    }
    #[test]
    fn test_complex_manifest() {
        let manifest: Result<Manifest, serde_json::Error> = serde_json::from_str(
            r#"{
                "uris": ["https://example.com/test.jpg"],
                "label": {
                    "en": ["Some title"],
                    "de": ["Ein Titel"]
                },
                "summary": "A longer description",
                "rights": "A rights statement",
                "provider": ["A link to the resource's provider"],
                "homepage": ["https://example.com", {"id": "https://example.com", "type": "Text", "label": {"en": ["Example website"], "de": ["Beispielwebsite"]}}],
                "seeAlso": "https://example.com/image-dataset",
                "region": "0,0,300,500",
                "format": "image/jpeg",
                "metadata": [
                    {
                        "label": "location",
                        "value": {
                            "en": ["somewhere"],
                            "de": ["irgendwo"]
                        }
                    }
                ]
            }"#,
        );
        assert!(manifest.is_ok());
    }
}
