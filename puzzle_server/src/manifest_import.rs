//! Importing and parsing manifests from local or remove sources
//!
//! This module provides the functionalities to retrieve manifests from external sources

use std::{
    collections::HashSet,
    fs::{self, File},
    io::{BufReader, Read},
    path::Path,
    str,
    sync::Arc,
};

use crate::{
    custom_manifest,
    db::{self, models as db_models, Db},
    http_client::HttpClient,
    image::{download_image, get_technical_image_metadata, transform_image},
};
use anyhow::{bail, Context, Result};
use log::{error, info};
use serde::{Deserialize, Serialize};
use tokio::sync::Mutex;

use crate::iiif;

#[derive(Clone, Debug, Serialize, Deserialize)]
/// Represents a IIIF manifest independent of the actual IIIF Presentation API's version
#[serde(untagged)]
pub enum Manifest {
    /// Represents a IIIF Presentation API version 2 manifest
    V2(iiif::v2::Manifest),
    /// Represents a IIIF Presentation API version 3 manifest
    V3(iiif::v3::Manifest),
    /// Represents a custom metadata manifest
    Custom(custom_manifest::Manifest),
}

/// Downloads a manifest, extracts referenced image files and downloads
/// these files to the import directory. If the same manifest already exists in the DB, the
/// manifest won't be updated, unless `force_import` is set to `true`.
pub async fn get_images_from_manifest(
    manifest_uri: &str,
    db: Arc<Mutex<Db>>,
    client: Arc<HttpClient>,
    import_dir: &str,
    force_import: bool,
) -> Result<Option<db::models::Manifest>> {
    info!("Importing manifest from {}", manifest_uri);

    let manifest_uid = cityhasher::hash::<u32>(manifest_uri);
    let (manifest_body, manifest_hash) = parse_manifest(client.clone(), manifest_uri)
        .await
        .context("Parsing manifest failed")?;
    if force_import
        || db
            .lock()
            .await
            .modified_manifest(manifest_uid, manifest_hash)?
    {
        let images_creation_metadata = get_technical_image_metadata(&manifest_body);
        let mut db_images = vec![];
        for image_creation_metadata in images_creation_metadata {
            let image_path =
                match download_image(client.clone(), &image_creation_metadata, import_dir).await {
                    Ok(Some(ip)) => ip,
                    Ok(None) => {
                        info!("Image from IIIF manifest already imported");
                        continue;
                    }
                    Err(e) => bail!("Downloading image failed failed: {e}"),
                };

            let image = db_models::Image::new(
                &image_creation_metadata.uri,
                &image_path,
                &image_creation_metadata.title,
            );
            db_images.push(image);
            if let Some(transform) = image_creation_metadata.transform {
                if let Err(e) = transform_image(transform, &image_path) {
                    error!("Tranforming image {} failed: {}", &image_path, e);
                    continue;
                }
            }
        }
        let db_manifest = db::models::Manifest::new(
            manifest_uid,
            manifest_uri,
            manifest_body,
            db_images,
            manifest_hash,
        )?;
        db.lock().await.add_manifest(&db_manifest, true)?;
        info!("Importing manifest from {} finished", manifest_uri);
        Ok(Some(db_manifest))
    } else {
        info!("Manifest ignored as the same manifest is already in DB");
        Ok(None)
    }
}

/// Replaces manifest directories in configured manifest list with actual manifest files
pub fn expand_manifest_list(manifest_list: &Vec<String>) -> HashSet<String> {
    let mut manifests = HashSet::new();
    for manifest_path in manifest_list {
        if manifest_path.starts_with("http") {
            manifests.insert(manifest_path.clone());
        } else if Path::new(&manifest_path).is_dir() {
            manifests.extend(get_manifests_in_path(manifest_path));
        } else {
            manifests.insert(manifest_path.clone());
        }
    }
    manifests
}

/// Returns all JSON files in given directories
fn get_manifests_in_path(dir: &str) -> Vec<String> {
    let mut manifests = vec![];
    if let Ok(read_dir) = fs::read_dir(dir) {
        for entry in read_dir {
            if let Ok(e) = entry {
                if e.path().is_file() {
                    if let Some(file_name_str) = e.file_name().to_str() {
                        if file_name_str.ends_with("json") {
                            if let Some(s) = e.path().to_str() {
                                manifests.push(s.to_string());
                            } else {
                                error!("Can't handle file path {:?}", e.path());
                            }
                        }
                    } else {
                        error!("Can't convert file path {:?} to str", e.file_name());
                    }
                }
            } else {
                error!("Can't read entry {:?}", &entry);
            }
        }
    } else {
        error!("Can't read dir {}", &dir);
    }
    manifests
}

/// Downloads and parses IIIF manifest
async fn parse_manifest(client: Arc<HttpClient>, uri: &str) -> Result<(Manifest, u32)> {
    match uri {
        u if u.starts_with("http") => {
            let buf = client
                .download_resource(uri)
                .await
                .context("GET request failed")?;
            let bytes = &buf.to_bytes();
            let hash = cityhasher::hash::<u32>(bytes.slice(..));
            serde_json::from_slice(bytes)
                .map(|manifest| (manifest, hash))
                .context("Parsing body as manifest failed")
        }
        u if u.starts_with('/') => {
            let path = Path::new(&u);
            let mut buf_reader = BufReader::new(File::open(path)?);
            let mut string_buf = String::with_capacity(4096);
            buf_reader.read_to_string(&mut string_buf)?;
            let hash = cityhasher::hash::<u32>(&string_buf);
            serde_json::from_str(&string_buf)
                .map(|manifest| (manifest, hash))
                .context("Parsing body as manifest failed")
        }
        _ => bail!("URI ({}) has no supported schema (http or file)", uri),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use tempfile::*;

    #[test]
    fn test_expand_manifest_list() {
        let dir = tempdir().unwrap();
        for i in 1..=3 {
            let file_path = dir.path().join(format!("manifest{i}.json"));
            File::create(file_path).unwrap();
        }
        let manifests = expand_manifest_list(&vec![dir.path().to_str().unwrap().to_owned()]);
        assert_eq!(manifests.len(), 3);
    }
}
