//! Application settings utilities
//!
//! Essentially, this module provides the [`Config`] struct and the main function generating a
//! [`Config`] instance, [`read_settings`]. This struct contains all settings
//! which can be set by the user. A user can apply a specific setting via a command line argument,
//! an environment variable or a field in the configuration file, the formers taking precendence
//! over the latters. There is only one obvious exception: The path to the configuration file
//! (`-c`, `--config-file` or `CONFIG_FILE`) can't be set in the configuration file. The settings
//! of the different sources are collected in [`InputConfig`], which has the same fields as
//! [`Config`] except that all field values are wrapped in an [`Option`]. If none of the three
//! sources provide a non-[`Option::None`] value for a certain setting, a default is set when the
//! [`Config`] instance is built.

use std::{
    env,
    fmt::Display,
    fs::File,
    io::{BufReader, Read},
    net::SocketAddr,
};

use anyhow::{anyhow, bail};
use clap::{Parser, ValueEnum};
use log::{error, info};
use once_cell::sync::Lazy;
use regex::Regex;
use serde::Deserialize;

#[derive(Clone, Deserialize, Default, Parser)]
#[serde(rename_all = "camelCase")]
#[command(author, version, about, long_about = None)]
#[clap(rename_all = "kebab-case")]
/// Contains all required settings to properly run the app
///
/// See
/// [`main.example.toml`](https://gitlab.switch.ch/ub-unibas/puzzle-app/backend/-/raw/main/puzzle_server/configs/main.example.toml)
/// for an example of a file configuration.
struct InputConfig {
    /// Server's host address
    /// Defaults to `0.0.0.0:3000`
    #[serde(default)]
    #[arg(short = 'a', long, env, verbatim_doc_comment)]
    pub host: Option<SocketAddr>,
    /// Secret to access protected API endpoints
    /// All API endpoints are unprotected if no secret is set
    #[serde(default)]
    #[arg(long, env, verbatim_doc_comment)]
    pub api_secret: Option<String>,
    /// Arguments provided to the magick executable resizing the images
    #[serde(default)]
    #[arg(long, env, verbatim_doc_comment)]
    pub resize_args: Option<String>,
    /// Arguments provided to the magick executable cutting out the puzzle pieces
    #[serde(default)]
    #[arg(long, env)]
    pub cut_out_args: Option<String>,
    /// Path to internal DB
    /// Must be POSIX-compliant and can be relative or absolute
    /// Defaults to `db/`
    #[serde(default)]
    #[arg(long, env, verbatim_doc_comment)]
    pub db_dir: Option<String>,
    /// Contains links to manifest files or directories containing manifests. Supported manifest
    /// formats are IIIF manifests based on the presentation API v2 or v3 and the app-specifc
    /// model. Must be a POSIX-compliant absolute or relative path or a valid URI. Defaults to
    /// `null`
    #[serde(default)]
    #[arg(short = 'm', long, env, value_delimiter = ',', verbatim_doc_comment)]
    pub manifests: Option<Vec<String>>,
    /// Folders from where to import additional manifests
    /// Must be POSIX-compliant and is either relative or absolute
    /// Defaults to `null`
    #[serde(default)]
    #[arg(short = 'd', long, env, value_delimiter = ',', verbatim_doc_comment)]
    pub manifest_dirs: Option<Vec<String>>,
    /// Path to directory where the puzzles are created
    /// Must be POSIX-compliant and can be relative or absolute
    /// Defaults to `puzzles/`
    #[serde(default)]
    #[arg(long, env, verbatim_doc_comment)]
    pub puzzle_dir: Option<String>,
    /// Path to directory where the frontend data is stored
    /// Must be POSIX-compliant and is either relative or absolute
    /// Defaults to `null` (no frontend provided)
    #[serde(default)]
    #[arg(long, env, verbatim_doc_comment)]
    pub frontend_dir: Option<String>,
    /// List of thumbnail geometries
    /// See https://imagemagick.org/script/command-line-processing.php#geometry for supported formats
    /// Defaults are `340`, `748`, `1004`
    #[serde(default)]
    #[arg(short = 't', long, env, value_delimiter = ',', verbatim_doc_comment)]
    pub thumbnail_geometries: Option<Vec<String>>,
    /// List of puzzle geometries
    /// See https://imagemagick.org/script/command-line-processing.php#geometry for supported formats
    /// Defaults are `340`, `620`, `748`, `1004`, `1260`, `1516`
    #[serde(default)]
    #[arg(short = 'p', long, env, value_delimiter = ',', verbatim_doc_comment)]
    pub puzzle_geometries: Option<Vec<String>>,
    /// List of puzzle sizes
    /// Defaults are `9`, `16`, `25`, `36`
    #[serde(default)]
    #[arg(short = 's', long, env, value_delimiter = ',', verbatim_doc_comment)]
    pub puzzle_sizes: Option<Vec<String>>,
    /// Minimal size of pieces in pixels
    /// Expressed as `<x>x<y>`
    /// Defaults to `null` (no minimal size)
    #[serde(default)]
    #[arg(long, env, verbatim_doc_comment)]
    pub minimal_piece_size: Option<String>,
    /// Maximum size of pieces in pixels
    /// Expressed as `<x>x<y>`
    /// Defaults to `null` (no maximum size)
    #[serde(default)]
    #[arg(long, env, verbatim_doc_comment)]
    pub maximal_piece_size: Option<String>,
    /// Level of asymmetry in the puzzle pieces' contour
    /// Values from `0.0` to `30.0` allowed
    /// Defaults to `0.0`
    #[serde(default)]
    #[arg(short = 'j', long, env, verbatim_doc_comment)]
    pub jitter: Option<f32>,
    /// Size of pieces's tab
    /// Values from `10.0` to `30.0` allowed
    /// Defaults to `20.0`
    #[serde(default)]
    #[arg(long, env, verbatim_doc_comment)]
    pub tab_size: Option<f32>,
    /// Seed for initialising the random puzzle contour generator
    /// Values from `0` to `9999` allowed
    /// Defaults to `0`
    #[serde(default)]
    #[arg(long, env, verbatim_doc_comment)]
    pub seed: Option<u16>,
    /// How images are imported
    /// Defaults to `incremental`
    #[serde(skip_deserializing, default)]
    #[arg(short, long, value_enum, verbatim_doc_comment)]
    pub import_mode: Option<ImportMode>,
    /// Path to configuration file
    #[serde(skip_deserializing)]
    #[arg(short = 'c', long, env)]
    pub config_file: Option<String>,
}

/// Holds application-wide settings
#[derive(Clone, Debug)]
pub struct Config {
    /// Server's host address. Defaults to `0.0.0.0:3000`
    pub host: SocketAddr,
    /// Secret to access protected API endpoints. Defaults to [Option::None]
    pub api_secret: Option<String>,
    /// `magick`'s arguments for resizing the images
    pub resize_args: String,
    /// Arguments provided to the magick executable for cutting out the puzzle pieces
    pub cut_out_args: String,
    /// Arguments provided to the magick executable for cutting out the puzzle pieces
    pub import_dir: String,
    /// Path to internal DB. Must be POSIX-compliant and can be relative or absolute. Defaults
    /// to `db/`
    pub db_dir: String,
    /// Contains links to manifest files or directories containing manifests. Supported manifest
    /// formats are IIIF manifests based on the presentation API v2 or v3 and the app-specifc
    /// model. Must be a POSIX-compliant absolute or relative path or a valid URI. Defaults to
    /// `null`
    pub manifests: Vec<String>,
    /// Path to directory where the puzzles are created. Must be POSIX-compliant and can be relative or absolute. Defaults to `puzzles/`
    pub puzzle_dir: String,
    /// Path to directory where the frontend data is stored. Must be POSIX-compliant and is either relative or absolute. Defaults to `null` (no frontend provided)
    pub frontend_dir: Option<String>,
    /// List of thumbnail geometries. See <https://imagemagick.org/script/command-line-processing.php#geometry> for a list of allowed forms. Defaults to `340`, `748`, `1004`
    pub thumbnail_geometries: Vec<String>,
    /// List of puzzle geometries. See <https://imagemagick.org/script/command-line-processing.php#geometry> for a list of allowed forms. Defaults to `340`, `620`, `748`, `1004`, `1260`, `1516`
    pub puzzle_geometries: Vec<String>,
    /// List of puzzle sizes. Defaults to `9`, `16`, `25`, `36`
    pub puzzle_sizes: Vec<PuzzleSize>,
    /// Minimal size of pieces in pixels, expressed as `<x>x<y>`. Defaults to `null` (no minimal
    /// size)
    pub minimal_piece_size: Option<(usize, usize)>,
    /// Maximal size of pieces in pixels, expressed as `<x>x<y>`. Defaults to `null` (no maximal
    /// size)
    pub maximal_piece_size: Option<(usize, usize)>,
    /// Level of asymmetry in the puzzle pieces' contour. Values from `0.0` to `30.0`, defaults to `0.0`
    pub jitter: f32,
    /// Size of pieces's tab. Values from `10.0` to `30.0` allowed, defaults to `20.0`
    pub tab_size: f32,
    /// Seed for initialising the random puzzle contour generator. Values from `0` to `9999`, defaults
    /// to `0`
    pub seed: u16,
    /// How images are imported. See [`ImportMode`] for more information. Defaults to `incremental`
    pub import_mode: ImportMode,
}

impl Config {
    /// Builds a [`Config`] instance by merging relevant command line arguments, environment variables and configuration file fields. If no non-[`Option::None`] values can be found for a setting, a default value is set.
    pub(self) fn build(cli_config: InputConfig, file_config: InputConfig) -> anyhow::Result<Self> {
        Ok(Self {
            host: cli_config
                .host
                .or(file_config.host)
                .unwrap_or("0.0.0.0:3000".parse().unwrap()),
            api_secret: cli_config.api_secret.or(file_config.api_secret),
            resize_args: cli_config
                .resize_args
                .or(file_config.resize_args)
                .unwrap_or("{{input_file}} -resize {{geometry}} {{output_file}}".to_string()),
            cut_out_args: cli_config
            .cut_out_args
            .or(file_config.cut_out_args)
            .unwrap_or("{{input_file}} ( +clone -fill Black -colorize 100 -fill White -draw {{svg_path}} ) -alpha off -compose CopyOpacity -composite -trim +repage ( +clone -channel A -separate +channel -negate -background black -virtual-pixel background -blur 0x2 -shade 120x21.78 -contrast-stretch 0% +sigmoidal-contrast 7x50%  -fill grey50 -colorize 10% +clone +swap -compose overlay -composite ) -compose In -composite {{shadow_layers}} -background none -compose DstOver -mosaic {{output_file}}".to_string()),
            import_dir: build_absolute_path("import/".to_string())?,
            db_dir: build_absolute_path(cli_config.db_dir.or(file_config.db_dir).unwrap_or("db/".to_string()))?,
            manifests: cli_config.manifests.or(file_config.manifests).unwrap_or_default().into_iter().filter_map(|ml| build_absolute_path(ml).ok()).collect(),
            puzzle_dir: build_absolute_path(cli_config.puzzle_dir.or(file_config.puzzle_dir).unwrap_or("puzzles/".to_string()))?,
            frontend_dir: cli_config.frontend_dir.or(file_config.frontend_dir).and_then(|p| build_absolute_path(p).ok()),
            thumbnail_geometries: cli_config.thumbnail_geometries.or(file_config.thumbnail_geometries).unwrap_or(["340", "748", "1004"]
            .iter()
            .map(|s| (*s).to_string())
            .collect()),
            puzzle_geometries: cli_config.puzzle_geometries.or(file_config.puzzle_geometries).unwrap_or(["340", "620", "748", "1004", "1260", "1516"]
            .iter()
            .map(|s| (*s).to_string())
            .collect()),
            puzzle_sizes: cli_config
                .puzzle_sizes
                .or(file_config.puzzle_sizes)
                .map_or([9, 16, 25, 36]
                    .iter()
                    .map(|num| PuzzleSize::Total(*num as usize))
                    .collect(), |ps| ps
                    .iter()
                    .filter_map(|e| str_to_puzzle_size(e).ok())
                    .collect()),
            minimal_piece_size: cli_config.minimal_piece_size.or(file_config.minimal_piece_size).and_then(|mp| str_to_two_usize(&mp).ok()),
            maximal_piece_size: cli_config.maximal_piece_size.or(file_config.maximal_piece_size).and_then(|mp| str_to_two_usize(&mp).ok()),
            jitter: cli_config.jitter.or(file_config.jitter).unwrap_or(0.0),
            tab_size: cli_config.tab_size.or(file_config.tab_size).unwrap_or(20.0),
            seed: cli_config.seed.or(file_config.seed).unwrap_or(0),
            import_mode: cli_config.import_mode.or(file_config.import_mode).unwrap_or(ImportMode::Incremental),
        })
    }
}

/// Number of pieces in puzzle. Either as a total number or as the number of pieces in a row and a
/// column, respectively
#[derive(Clone, Debug, Deserialize)]
pub enum PuzzleSize {
    Total(usize),
    RowsCols((usize, usize)),
}

/// Defines the way images are imported and puzzles are created. Defaults to
/// [`ImportMode::Incremental`]
#[derive(ValueEnum, Clone, Debug, Default, Parser, PartialEq, Eq)]
#[clap(rename_all = "kebab-case")]
pub enum ImportMode {
    /// Deactivates import
    Off,
    /// Imports only images newly added to configuration
    #[default]
    Incremental,
    /// Imports images and create puzzles regardless of preexisting objects
    Force,
    /// Resets images as defined in configuration
    Reset,
}

impl Display for ImportMode {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Force => write!(f, "force"),
            Self::Off => write!(f, "off"),
            Self::Incremental => write!(f, "incremental"),
            Self::Reset => write!(f, "reset"),
        }
    }
}

/// Converts a string like `632x321`, `12x4311` or `3x9` to an ordered pair of usize values
fn str_to_two_usize(s: &str) -> anyhow::Result<(usize, usize)> {
    let x_y: Lazy<Regex> = Lazy::new(|| Regex::new(r"^(?<x>[0-9]+)x(?<y>[0-9]+)$").unwrap());
    if x_y.is_match(s) {
        let x = get_usize("x", s, &x_y).unwrap();
        let y = get_usize("y", s, &x_y).unwrap();
        Ok((x, y))
    } else {
        let err_msg = format!("Invalid puzzle size format {}", &s);
        error!("{}", &err_msg);
        Err(anyhow!("{}", &err_msg))
    }
}

/// Builds an absolute paths given a relative path and the current working directory
fn build_absolute_path(path: String) -> anyhow::Result<String> {
    if path.starts_with('/') || path.starts_with("http") {
        Ok(path)
    } else if path.starts_with("file://") {
        Ok(path.replace("file://", ""))
    } else {
        let mut pwd = env::current_dir()?;
        pwd.push(path);
        Ok(pwd.to_string_lossy().to_string())
    }
}

/// Converts a string to a [`PuzzleSize`] variant
fn str_to_puzzle_size(s: &str) -> anyhow::Result<PuzzleSize> {
    let x_y: Lazy<Regex> = Lazy::new(|| Regex::new(r"^(?<x>[0-9]+)x(?<y>[0-9]+)$").unwrap());
    match s {
        total if total.parse::<usize>().is_ok() => {
            Ok(PuzzleSize::Total(total.parse::<usize>().unwrap()))
        }
        row_cols if x_y.is_match(row_cols) => Ok(PuzzleSize::RowsCols((
            get_usize("x", row_cols, &x_y).unwrap(),
            get_usize("y", row_cols, &x_y).unwrap(),
        ))),
        _ => Err(anyhow!("Invalid puzzle size format {}", &s)),
    }
}

/// Get a single usize value from strings like `2432x434`
fn get_usize(group_name: &str, haystack: &str, re: &Lazy<Regex>) -> Option<usize> {
    re.captures(haystack)
        .and_then(|c| c.name(group_name))
        .map(|g| g.as_str())
        .and_then(|s| s.parse::<usize>().ok())
}

/// Creates a [`Config`] instance from the different possible input sources
pub fn read_settings() -> anyhow::Result<Config> {
    let cli_config = InputConfig::parse();
    let path = cli_config
        .config_file
        .clone()
        .unwrap_or("configs/main.toml".to_string());
    let file_config = if let Ok(file) = File::open(&path) {
        let mut reader = BufReader::new(file);
        let mut buf = String::new();
        reader.read_to_string(&mut buf)?;
        toml::from_str(&buf)?
    } else {
        info!("No config file found under {}", &path);
        InputConfig::default()
    };
    let config = Config::build(cli_config, file_config)?;
    config_checks(config)
}

/// Performs validation on some [`Config`] fields
fn config_checks(config: Config) -> anyhow::Result<Config> {
    if config.tab_size < 10.0 || config.tab_size > 30.0 {
        bail!("Tab size must be between 10.0 and 30.0");
    }
    if config.jitter < 0.0 || config.jitter > 13.0 {
        bail!("Jitter must be between 0.0 and 13.0");
    }
    if config.seed > 9999 {
        bail!("Seed number has to be between 0 and 9999");
    }
    Ok(config)
}
