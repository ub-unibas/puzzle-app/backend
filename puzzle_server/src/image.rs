//! Handling images and their metadata
//!
//! This module provides the utils to import (including transform) images and to export image
//! metadata in a custom format.

use anyhow::{anyhow, bail, Context, Result};
use log::{debug, error};
use once_cell::sync::Lazy;
use regex::Regex;
use std::fs::{self, File};
use std::io::{BufWriter, Write};
use std::path::{Path, PathBuf};
use std::process;
use std::sync::Arc;

use crate::custom_manifest::MayBeStructuredMetadata;
use crate::http_client::HttpClient;
use crate::iiif::v3::{Agent, Homepage, ImageApiSelector, SeeAlso, StructuredMetadata};
use crate::manifest_import::Manifest;
use crate::{custom_manifest, iiif};

pub mod models {
    //! Models for describing images

    use crate::iiif::v3::{Agent, Homepage, ImageApiSelector, SeeAlso, StructuredMetadata};
    use std::collections::HashMap;

    use serde::{Deserialize, Serialize};

    /// Contains image metadata
    #[derive(Default, Deserialize, Serialize, Clone, Debug)]
    #[serde(rename_all = "camelCase")]
    pub struct ImageMetadata {
        /// Image UID
        pub uid: u32,
        /// Optional image description
        #[serde(skip_serializing_if = "Option::is_none")]
        pub summary: Option<HashMap<String, Vec<String>>>,
        /// Image name
        pub label: HashMap<String, Vec<String>>,
        /// Rights statement or link to one
        #[serde(skip_serializing_if = "Option::is_none")]
        pub rights: Option<String>,
        /// Additional metadata
        #[serde(skip_serializing_if = "Option::is_none")]
        pub metadata: Option<Vec<StructuredMetadata>>,
        /// Metadata which must be displayed
        #[serde(skip_serializing_if = "Option::is_none")]
        pub required_statement: Option<StructuredMetadata>,
        /// MIME format
        #[serde(skip_serializing_if = "Option::is_none")]
        pub format: Option<String>,
        /// Resource's homepage
        #[serde(skip_serializing_if = "Option::is_none")]
        pub homepage: Option<Vec<Homepage>>,
        /// Related resources
        #[serde(skip_serializing_if = "Option::is_none")]
        pub see_also: Option<SeeAlso>,
        /// Resource owner
        #[serde(skip_serializing_if = "Option::is_none")]
        pub provider: Option<Vec<Agent>>,
    }

    /// Contains information needed to save and transform images
    #[derive(Debug)]
    pub struct ImageCreationMetadata {
        /// Image's uid
        pub uid: String,
        /// Image's title
        pub title: HashMap<String, Vec<String>>,
        /// URI (local file path) of the image
        pub uri: String,
        /// Image's file extension
        pub file_ext: String,
        /// Information on how to transform the image
        pub transform: Option<ImageApiSelector>,
    }
}

impl From<&iiif::v3::Manifest> for Vec<models::ImageMetadata> {
    fn from(value: &iiif::v3::Manifest) -> Self {
        value
            .items
            .iter()
            .flat_map(|canvas| canvas.items.clone())
            .flat_map(|anno_page| anno_page.items)
            .map(|anno| anno.body)
            .map(|res| models::ImageMetadata {
                uid: cityhasher::hash::<u32>(&res.uri()),
                summary: value.summary.clone().map(std::convert::Into::into),
                label: value.label.clone().into(),
                rights: value.rights.clone(),
                metadata: value.metadata.clone(),
                required_statement: value.required_statement.clone(),
                format: match res {
                    iiif::v3::Resource::Content(ref c) => c.format.clone(),
                    iiif::v3::Resource::SpecificResource(ref sr) => sr.source.format.clone(),
                },

                provider: value.provider.clone(),
                homepage: value.homepage.clone(),
                see_also: value.see_also.clone(),
            })
            .collect()
    }
}

impl From<&iiif::v2::Manifest> for Vec<models::ImageMetadata> {
    fn from(value: &iiif::v2::Manifest) -> Self {
        value
            .sequences
            .iter()
            .flat_map(|seq| seq.canvases.clone())
            .flat_map(|canvas| canvas.images)
            .map(|image| image.resource)
            .map(|res| models::ImageMetadata {
                uid: cityhasher::hash::<u32>(&res.uri()),
                summary: value.description.clone().map(std::convert::Into::into),
                label: value.label.clone().into(),
                rights: value.license.clone().or_else(|| value.attribution.clone()),
                metadata: value.metadata.clone().map(|m| {
                    m.into_iter()
                        .map(std::convert::Into::into)
                        .collect::<Vec<iiif::v3::StructuredMetadata>>()
                }),
                required_statement: None,
                format: match res {
                    iiif::v2::Resource::Content(ref c) => c.format.clone(),
                    iiif::v2::Resource::SpecificResource(ref sr) => sr.full.format.clone(),
                },
                provider: None,
                homepage: None,
                see_also: None,
            })
            .collect()
    }
}

impl From<&custom_manifest::Manifest> for Vec<models::ImageMetadata> {
    fn from(value: &custom_manifest::Manifest) -> Self {
        value
            .uris
            .iter()
            .map(|res| models::ImageMetadata {
                uid: cityhasher::hash::<u32>(&res),
                summary: value.summary.clone().map(std::convert::Into::into),
                label: value.label.clone().into(),
                rights: value.rights.clone(),
                metadata: match value.metadata {
                    Some(MayBeStructuredMetadata::SimpleMap(ref hm)) => Some(
                        hm.clone()
                            .into_iter()
                            .map(|(k, v)| StructuredMetadata {
                                value: iiif::v3::MayBeI18NString::String(v),
                                label: iiif::v3::MayBeI18NString::String(k),
                            })
                            .collect(),
                    ),
                    Some(MayBeStructuredMetadata::StructuredMetadata(ref sm)) => Some(sm.clone()),
                    _ => None,
                },
                required_statement: None,
                format: value.format.clone(),
                provider: value.provider.clone().map(|p| {
                    p.iter()
                        .map(|e| Agent {
                            id: "https://localhost/agent".to_string(),
                            provider_type: "Agent".to_string(),
                            label: e.clone(),
                            homepage: None,
                            logo: None,
                            see_also: None,
                        })
                        .collect()
                }),
                homepage: value.homepage.clone().map(|h| {
                    h.iter()
                        .map(|e| match e {
                            custom_manifest::MayBeHomepage::Uri(u) => Homepage {
                                id: u.clone(),
                                content_type: "Unknown".to_string(),
                                label: iiif::v3::MayBeI18NString::String(u.clone()),
                                format: None,
                                language: None,
                            },
                            custom_manifest::MayBeHomepage::Homepage(h) => h.clone(),
                        })
                        .collect()
                }),
                see_also: value.see_also.clone().map(|s| match s {
                    custom_manifest::MayBeSeeAlso::Uri(u) => SeeAlso {
                        id: u,
                        content_type: "Unknown".to_string(),
                        label: None,
                        format: None,
                        profile: None,
                    },
                    custom_manifest::MayBeSeeAlso::SeeAlso(s) => s,
                }),
            })
            .collect()
    }
}

impl From<&iiif::v3::Manifest> for Vec<models::ImageCreationMetadata> {
    fn from(value: &iiif::v3::Manifest) -> Self {
        value
            .items
            .iter()
            .flat_map(|canvas| canvas.items.clone())
            .flat_map(|anno_page| anno_page.items)
            .map(|anno| anno.body)
            .map(|res| {
                let uri = res.uri();
                models::ImageCreationMetadata {
                    uid: cityhasher::hash::<u32>(&uri).to_string(),
                    title: value.label.clone().into(),
                    file_ext: uri.split('.').last().unwrap_or("").to_string(),
                    uri: uri.to_string(),
                    transform: match res {
                        iiif::v3::Resource::SpecificResource(ref sr)
                            if sr.selector.selector_type.contains("ImageApiSelector") =>
                        {
                            Some(sr.selector.clone())
                        }
                        _ => None,
                    },
                }
            })
            .collect()
    }
}

impl From<&iiif::v2::Manifest> for Vec<models::ImageCreationMetadata> {
    fn from(value: &iiif::v2::Manifest) -> Self {
        value
            .sequences
            .iter()
            .flat_map(|seq| seq.canvases.clone())
            .flat_map(|canvas| canvas.images)
            .map(|image| image.resource)
            .map(|res| {
                let uri = res.uri();
                models::ImageCreationMetadata {
                    uid: cityhasher::hash::<u32>(&uri).to_string(),
                    title: value.label.clone().into(),
                    file_ext: uri.split('.').last().unwrap_or("").to_string(),
                    uri: uri.to_string(),
                    transform: match res {
                        iiif::v2::Resource::SpecificResource(ref sr)
                            if sr.selector.selector_type.contains("ImageApiSelector") =>
                        {
                            Some(sr.selector.clone().into())
                        }
                        _ => None,
                    },
                }
            })
            .collect()
    }
}

impl From<&custom_manifest::Manifest> for Vec<models::ImageCreationMetadata> {
    fn from(value: &custom_manifest::Manifest) -> Self {
        value
            .uris
            .iter()
            .map(|uri| models::ImageCreationMetadata {
                uid: cityhasher::hash::<u32>(&uri).to_string(),
                title: value.label.clone().into(),
                file_ext: uri.split('.').last().unwrap_or("").to_string(),
                uri: uri.clone(),
                transform: Some(ImageApiSelector {
                    selector_type: "Custom".to_string(),
                    region: value.region.clone(),
                    size: None,
                    rotation: value.rotation.clone(),
                    quality: None,
                    format: None,
                }),
            })
            .collect()
    }
}

/// Creates directory for temporarily saving imported manifests, images and puzzles
pub fn create_import_folder(import_dir: &str) -> Result<()> {
    let path = Path::new(import_dir);
    if !path.exists() {
        fs::create_dir(path)
            .with_context(|| format!("Creating import dir at {import_dir} failed"))?;
        debug!("Import dir at {} created", import_dir);
    } else {
        debug!("Import dir at {} already exists", import_dir);
    }
    Ok(())
}

/// Downloads image to import folder and write metadata to DB
pub async fn download_image(
    client: Arc<HttpClient>,
    iiif_content_info: &models::ImageCreationMetadata,
    import_folder_path: &str,
) -> Result<Option<String>> {
    let mut path_buf = PathBuf::new();
    path_buf.push(import_folder_path);
    path_buf.push(&iiif_content_info.uid);
    path_buf.set_extension(&iiif_content_info.file_ext);
    let path = path_buf.as_path();
    if !path.exists() {
        debug!("Getting image from {}", &iiif_content_info.uri);
        match &iiif_content_info.uri {
            u if u.starts_with("http") => {
                let buf = client
                    .download_resource(&iiif_content_info.uri)
                    .await
                    .with_context(|| format!("GET request for {} failed", &u))?;
                let file = File::create(path).context("Creating file path failed")?;
                let mut buf_writer = BufWriter::new(file);
                let img_bytes = &buf.to_bytes();
                buf_writer
                    .write_all(img_bytes)
                    .context("Writing image buffer failed")?;
                debug!(
                    "External image file {} (size: {}) written to {}",
                    u,
                    img_bytes.len(),
                    path.to_str().unwrap_or("<unknown path>")
                );
            }
            u if u.starts_with("file://") => {
                let u = u.replace("file://", "");
                let size = fs::copy(&u, path).context("Copying local file failed")?;
                debug!(
                    "Local image file {} (size: {}) written to {}",
                    &u,
                    size,
                    path.to_str().unwrap_or("<unknown path>")
                );
            }
            u if u.starts_with('/') => {
                fs::copy(u, path).context("Copying local file failed")?;
                let size = fs::copy(&u, path)?;
                debug!(
                    "Local image file {} (size: {}) written to {}",
                    &u,
                    size,
                    path.to_str().unwrap_or("<unknown path>")
                );
            }
            u => {
                let msg = format!("URI ({}) has no supported schema (http or file)", &u);
                error!("{}", &msg);
                bail!(msg);
            }
        }
        Ok(Some(path_buf.to_string_lossy().to_string()))
    } else {
        debug!(
            "Ignoring image from {} as file already exists locally",
            &iiif_content_info.uri
        );
        Ok(None)
    }
}

/// Extracts metadata from manifest required to save and optionally transform an image
pub fn get_technical_image_metadata(manifest: &Manifest) -> Vec<models::ImageCreationMetadata> {
    match manifest {
        Manifest::V2(m) => m.into(),
        Manifest::V3(m) => m.into(),
        Manifest::Custom(m) => m.into(),
    }
}

/// Retrieves the relevant image metadata from a concrete instance of a
/// [`crate::manifest_import::Manifest`]
pub fn get_image_metadata(manifest: &Manifest) -> Vec<models::ImageMetadata> {
    match manifest {
        crate::manifest_import::Manifest::V2(ref m) => m.into(),
        crate::manifest_import::Manifest::V3(ref m) => m.into(),
        crate::manifest_import::Manifest::Custom(ref m) => m.into(),
    }
}

/// Transforms (resizes, rotates) image if transformation is defined in manifest
pub fn transform_image(rules: ImageApiSelector, path: &str) -> Result<()> {
    let mut im_cmd = vec!["mogrify".to_string()];
    resize_image(rules.region, &mut im_cmd)?;
    rotate_image(rules.rotation, &mut im_cmd)?;
    im_cmd.push(path.to_string());
    if let Some(args) = shlex::split(&im_cmd.join(" ")) {
        let output = process::Command::new("magick")
            .args(args)
            .output()
            .context("Execution of `magick` command failed")?;
        if !output.status.success() {
            let stdout_msg = String::from_utf8(output.stdout)
                .context("Reading in stdout from executing `magick` command failed")?;
            let stderr_msg = String::from_utf8(output.stderr)
                .context("Reading in stderr from executing `magick` command failed")?;
            let msg = format!("Error transforming image (error code {})", output.status);
            error!("{}", &msg);
            debug!("STDOUT: {}", &stdout_msg);
            debug!("STDERR: {}", &stderr_msg);
            bail!(msg);
        }
        debug!("Image {} successfully transformed", &path);
        Ok(())
    } else {
        Err(anyhow!("convert args building failed"))
    }
}

/// Resizes image
fn resize_image(region: Option<String>, im_cmd: &mut Vec<String>) -> Result<()> {
    let pct: Lazy<Regex> = Lazy::new(|| {
        Regex::new(r"^pct:(?<x>[0-9.]+),(?<y>[0-9.]+),(?<w>[0-9.]+),(?<h>[0-9.]+)$").unwrap()
    });
    let px: Lazy<Regex> = Lazy::new(|| {
        Regex::new(r"^(?<x>[0-9.]+),(?<y>[0-9.]+),(?<w>[0-9.]+),(?<h>[0-9.]+)$").unwrap()
    });
    match region {
        Some(r) if r == "full" => {}
        Some(r) if r == "square" => {
            im_cmd.append(&mut vec![
                "-gravity".to_string(),
                "center".to_string(),
                "-extent".to_string(),
                "'%[fx:h<w?h:w]x%[fx:h<w?h:w]'".to_string(),
            ]);
        }
        Some(r) => {
            if let Some(c) = px.captures(&r) {
                if let Some(v) = vec![c.name("x"), c.name("y"), c.name("w"), c.name("h")]
                    .into_iter()
                    .map(|e| e.map(|x| x.as_str()))
                    .collect::<Option<Vec<_>>>()
                {
                    im_cmd.push("-crop".to_string());
                    let crop_values = format!(
                        "{}x{}+{}+{}",
                        v.get(2).unwrap(),
                        v.get(3).unwrap(),
                        v.first().unwrap(),
                        v.get(1).unwrap()
                    );
                    im_cmd.push(crop_values);
                }
            } else if let Some(c) = pct.captures(&r) {
                if let Some(v) = vec![c.name("x"), c.name("y"), c.name("w"), c.name("h")]
                    .into_iter()
                    .map(|e| e.map(|x| x.as_str()))
                    .collect::<Option<Vec<_>>>()
                {
                    im_cmd.append(&mut vec![
                        "-set".to_string(),
                        "page".to_string(),
                        format!(
                            "-%[fx:w*{}]-%[fx:h*{}]",
                            v.first().unwrap().parse::<f32>().with_context(|| format!(
                                "Parsing {} as f32 failed",
                                v.first().unwrap()
                            ))? / 100.,
                            v.get(1).unwrap().parse::<f32>().with_context(|| format!(
                                "Parsing {} as f32 failed",
                                v.get(1).unwrap()
                            ))? / 100.
                        ),
                        "-crop".to_string(),
                        format!("{}%x{}%+0+0", v.get(2).unwrap(), v.get(3).unwrap(),),
                    ]);
                }
            }
        }
        None => {}
    };
    Ok(())
}

/// Rotates image
fn rotate_image(rotation: Option<String>, im_cmd: &mut Vec<String>) -> Result<()> {
    match rotation {
        Some(r) if r.starts_with('!') => {
            im_cmd.push("-flop".to_string());
            im_cmd.push((&r)[1..].to_string());
        }
        Some(r) => {
            im_cmd.push("-rotate".to_string());
            im_cmd.push(r);
        }
        None => {}
    };
    Ok(())
}
