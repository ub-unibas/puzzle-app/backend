//! Maintains the persistent content of the application.
//!
//! ## Namespaces
//!
//! Namespaces help to keep related data together. These namespaces are used:
//!
//! - `manifests`: Imported manifests with metadata, sorted by the manifests' uids.
//! - `scores`: Information on the achieved scores, sorted by the puzzles' ids. See [`Score`] for
//! more information
//!

use crate::manifest_import::Manifest;

use std::{
    collections::{HashMap, HashSet},
    io,
    ops::Deref,
    thread, time,
};

use anyhow::{anyhow, bail, Context, Result};
use log::{debug, error};
use sled::IVec;

pub mod models {
    //! Models used to serialise data in DB

    use serde::{Deserialize, Serialize};
    use std::collections::HashMap;

    use crate::manifest_import;

    /// Manifest information which is serialised in the `manifests` namespace
    #[derive(Deserialize, Serialize, Clone, Debug)]
    pub struct Manifest {
        /// Manifest's unique identifier
        pub uid: u32,
        /// URI of the original manifest
        pub uri: String,
        /// Manifest's title
        pub label: HashMap<String, Vec<String>>,
        /// Timestamp of the last modification
        pub timestamp: i64,
        /// Import status. See [`ImportStatus`] for more information
        pub status: ImportStatus,
        /// List of attached images
        pub images: Vec<Image>,
        /// Manifest's body (i.e. the entire manifest)
        pub body: manifest_import::Manifest,
        /// Hashed body
        pub manifest_hash: u32,
    }

    /// Image information which is serialised in the `images` namespace
    #[derive(Deserialize, Serialize, Clone, Debug)]
    pub struct Image {
        /// Image uid
        pub uid: u32,
        /// Image title
        pub title: HashMap<String, Vec<String>>,
        /// Path to original image file
        pub original_path: String,
        /// Paths to resized images
        pub resized_images: Vec<ResizedImage>,
        /// Image's thumbnails
        pub thumbnails: Vec<ResizedImage>,
    }

    /// From where the image has been imported
    #[derive(Deserialize, Serialize, Clone, Debug)]
    pub enum ImageOrigin {
        /// From local import folder
        ImportFolder,
        /// From IIIF manifest
        Iiif(String),
    }

    /// Status of image's import
    #[derive(Deserialize, Serialize, Eq, PartialEq, Clone, Debug)]
    pub enum ImportStatus {
        /// Pending operation
        Pending,
        /// Puzzles successfully created
        Finished,
        /// Last import ended in errors
        Error(String),
    }

    /// Information on a resized image
    #[derive(Deserialize, Serialize, Clone, Debug)]
    pub struct ResizedImage {
        /// UID of thumbnail's dimension
        pub dimension_uid: usize,
        /// Path to image file
        pub image_path: String,
        /// Image's width and height
        pub dimension: (f32, f32),
    }

    /// Score which is serialised in the `scores` namespace
    #[derive(Deserialize, Serialize, PartialEq, Eq, Hash, Clone)]
    pub struct GameScore {
        /// Unique identifier
        pub uid: u32,
        /// User name
        pub name: String,
        /// Time in secs to finish the jigsaw puzzle
        pub secs: usize,
        /// Timestamp of the game
        pub timestamp: i64,
    }
}

impl models::Manifest {
    /// Creates a new [`models::Manifest`]
    pub fn new(
        uid: u32,
        manifest_uri: &str,
        manifest_body: Manifest,
        images: Vec<models::Image>,
        manifest_hash: u32,
    ) -> Result<Self> {
        let uri = manifest_uri.to_string();
        let label: HashMap<String, Vec<String>> = match manifest_body {
            Manifest::V2(ref m) => m.label.clone().into(),
            Manifest::V3(ref m) => m.label.clone().into(),
            Manifest::Custom(ref m) => m.label.clone().into(),
        };
        let timestamp = chrono::Utc::now().timestamp();
        let status = models::ImportStatus::Pending;
        Ok(Self {
            uid,
            uri,
            label,
            timestamp,
            status,
            images,
            body: manifest_body,
            manifest_hash,
        })
    }
}

impl models::Image {
    /// Creates a new [`models::Image`]
    pub fn new(uri: &str, local_path: &str, title: &HashMap<String, Vec<String>>) -> Self {
        let uid = cityhasher::hash::<u32>(uri);
        let title = title.to_owned();
        let path = local_path.to_string();
        Self {
            uid,
            title,
            original_path: path,
            resized_images: vec![],
            thumbnails: vec![],
        }
    }
}

impl models::ImportStatus {
    /// Returns true if import status is error
    pub fn has_err(&self) -> bool {
        matches!(self, Self::Error(_))
    }
}

/// Wrapper around [`sled::Db`]. Provides methods to add, modify and delete data in DB.
#[derive(Clone)]
pub struct Db(sled::Db);

impl Db {
    /// Opens database
    pub fn open(path: &str) -> Result<Self> {
        let mut retries = 0;
        loop {
            match sled::open(path) {
                Ok(db) => return Ok(Self(db)),
                Err(e) => {
                    if let sled::Error::Io(ref io_e) = e {
                        if io_e.kind() == io::ErrorKind::WouldBlock {
                            if retries > 5 {
                                bail!("Opening sled DB failed: Too many retries to unlock DB");
                            }
                            retries += 1;
                            thread::sleep(time::Duration::from_secs(10));
                            continue;
                        }
                    }
                    bail!("Opening sled DB failed: {}", e);
                }
            }
        }
    }

    /// Registers manifest in DB
    pub fn add_manifest(
        &mut self,
        manifest: &models::Manifest,
        overwrite: bool,
    ) -> Result<Option<()>> {
        self.set_manifest(manifest, manifest.uid, overwrite)
    }

    /// Registers manifest in DB
    pub fn set_manifest(
        &mut self,
        manifest: &models::Manifest,
        uid: u32,
        overwrite: bool,
    ) -> Result<Option<()>> {
        debug!(
            "Adding manifest {} with uid {} to DB namespace `manifests`",
            &manifest.uri, &uid
        );
        let manifests_ns = self
            .open_tree("manifests")
            .context("Opening DB namespace `manifests` failed")?;
        if overwrite
            || !manifests_ns
                .contains_key(uid.to_ne_bytes())
                .unwrap_or(false)
        {
            manifests_ns
                .insert(
                    uid.to_ne_bytes(),
                    serde_json::to_vec(manifest).context("Failed to serialise manifest")?,
                )
                .map_err(std::convert::Into::into)
                .map(|_| Some(()))
        } else {
            Ok(None)
        }
    }

    /// Returns a manifest by its uid
    pub fn get_manifest(&self, uid: u32) -> Result<Option<models::Manifest>> {
        let manifests_ns = self
            .open_tree("manifests")
            .context("Opening DB namespace `manifests` failed")?;
        let manifest = manifests_ns.get(uid.to_ne_bytes())?;
        Ok(match manifest {
            Some(v) => Some(serde_json::from_slice(&v)?),
            None => None,
        })
    }

    /// Returns all manifests
    pub fn get_manifests(&self) -> Result<Vec<models::Manifest>> {
        let manifests_ns = self
            .open_tree("manifests")
            .context("Opening DB namespace `manifests` failed")?;
        let mut manifests = vec![];
        for manifest in &manifests_ns {
            if let Ok(e) = manifest {
                let manifest: models::Manifest = serde_json::from_slice(&e.1)?;
                manifests.push(manifest);
            } else {
                error!("Couldn't read DB entry!");
            }
        }
        Ok(manifests)
    }

    /// Returns true if manifest has been updated or is not yet in DB
    pub fn modified_manifest(&self, uid: u32, hash: u32) -> Result<bool> {
        if let Some(manifest) = self.get_manifest(uid)? {
            Ok(manifest.manifest_hash != hash)
        } else {
            Ok(true)
        }
    }

    /// Returns an image by its uid
    pub fn get_image(&self, uid: u32) -> Result<Option<models::Image>> {
        let manifests = self.get_manifests()?;
        Ok(manifests
            .into_iter()
            .flat_map(|m| m.images)
            .find(|img| img.uid == uid))
    }

    /// Returns all images which have been successfully imported
    pub fn get_all_available_images(&self) -> Result<Vec<models::Image>> {
        let manifests = self.get_manifests()?;
        Ok(manifests
            .into_iter()
            .filter(|m| m.status == models::ImportStatus::Finished)
            .flat_map(|m| m.images)
            .collect())
    }

    /// Adds thumbnails to an already existing image
    pub fn add_thumbnails(
        &mut self,
        manifest_uid: u32,
        image_uid: u32,
        thumbnails: Vec<models::ResizedImage>,
    ) -> Result<Option<()>> {
        let manifests_ns = self
            .open_tree("manifests")
            .context("Opening DB namespace `manifests` failed")?;
        let manifest = manifests_ns.get(manifest_uid.to_ne_bytes())?;
        if manifest.is_none() {
            return Ok(None);
        }
        let manifest: models::Manifest = serde_json::from_slice(&manifest.unwrap())?;

        let images: Vec<models::Image> = manifest
            .images
            .into_iter()
            .map(|img| {
                if img.uid == image_uid {
                    models::Image {
                        thumbnails: thumbnails.clone(),
                        ..img
                    }
                } else {
                    img
                }
            })
            .collect();
        let new_manifest = models::Manifest { images, ..manifest };
        manifests_ns
            .insert(
                manifest_uid.to_ne_bytes(),
                serde_json::to_vec(&new_manifest).context("Failed to serialise new manifest")?,
            )
            .map_err(std::convert::Into::into)
            .map(|_| Some(()))
    }

    /// Adds a resized image to an already existing image entry
    pub fn add_resized_image(
        &mut self,
        manifest_uid: u32,
        image_uid: u32,
        resized_image: &Vec<models::ResizedImage>,
    ) -> Result<Option<()>> {
        let manifests_ns = self
            .open_tree("manifests")
            .context("Opening DB namespace `manifests` failed")?;
        let manifest = manifests_ns.get(manifest_uid.to_ne_bytes())?;
        if manifest.is_none() {
            return Ok(None);
        }
        let manifest: models::Manifest = serde_json::from_slice(&manifest.unwrap())?;

        let images: Vec<models::Image> = manifest
            .images
            .into_iter()
            .map(|img| {
                if img.uid == image_uid {
                    let mut resized_images = img.resized_images;
                    resized_images.extend(resized_image.to_owned());
                    models::Image {
                        resized_images,
                        ..img
                    }
                } else {
                    img
                }
            })
            .collect();
        let new_manifest = models::Manifest { images, ..manifest };
        manifests_ns
            .insert(
                manifest_uid.to_ne_bytes(),
                serde_json::to_vec(&new_manifest).context("Failed to serialise new manifest")?,
            )
            .map_err(std::convert::Into::into)
            .map(|_| Some(()))
    }

    /// Returns true if manifest has been imported successfully
    pub fn manifest_import_successful(&self, manifest_uri: &str) -> Result<bool> {
        let uid = cityhasher::hash::<u32>(manifest_uri);
        let manifests_ns = self
            .open_tree("manifests")
            .context("Opening DB namespace `manifests` failed")?;
        if let Some(manifest) = manifests_ns.get(uid.to_ne_bytes())? {
            let manifest: models::Manifest = serde_json::from_slice(&manifest)?;
            Ok(!manifest.status.has_err())
        } else {
            debug!(
                "No manifest with uid {} and uri {} found",
                uid, manifest_uri
            );
            Ok(false)
        }
    }

    /// Deletes a manifest with its uid
    pub fn delete_manifest(&self, uid: u32) -> Result<Option<()>> {
        debug!(
            "Deleting entry with uid {} from DB namespace `manifest`",
            &uid
        );
        let manifests_ns = self
            .open_tree("manifests")
            .context("Opening DB namespace `manifests` failed")?;
        manifests_ns
            .remove(uid.to_ne_bytes())
            .map(|_| Some(()))
            .map_err(std::convert::Into::into)
    }

    /// Deletes all manifests
    pub fn delete_all_manifests(&self) -> Result<()> {
        debug!("Deleting all entries in `manifests` namespace");
        let images_ns = self
            .open_tree("manifests")
            .context("Opening DB namespace `manifests` failed")?;
        images_ns.clear().map_err(std::convert::Into::into)
    }

    /// Updates [`models::ImportStatus`] for a manifest
    pub fn update_manifest_import_status(
        &mut self,
        uid: u32,
        new_status: models::ImportStatus,
    ) -> Result<Option<()>> {
        if let Some(manifest) = self.get_manifest(uid)? {
            debug!(
                "Updating import status of manifest with uid {} to {:?} in DB namespace `manifests`",
                &uid, &new_status,
            );
            let new_manifest = models::Manifest {
                status: new_status,
                ..manifest
            };
            self.set_manifest(&new_manifest, uid, true)?;
            Ok(Some(()))
        } else {
            Ok(None)
        }
    }

    /// Returns byte representation of a puzzle with its id
    fn get_scores_as_bytes(&self, puzzle_id: u32) -> Result<Option<IVec>> {
        let scores_ns = self
            .open_tree("scores")
            .context("Opening DB namespace `scores` failed")?;
        scores_ns
            .get(puzzle_id.to_ne_bytes())
            .with_context(|| format!("Fetching {puzzle_id} failed"))
    }

    /// Returns all scores of a puzzle with its id
    pub fn get_scores(&self, puzzle_id: u32) -> Result<Option<Vec<models::GameScore>>> {
        let scores = self.get_scores_as_bytes(puzzle_id)?;
        Ok(scores.and_then(|r| {
            serde_json::from_slice::<Vec<models::GameScore>>(r.as_ref())
                .context("Failed to deserialise result")
                .ok()
        }))
    }

    /// Returns scores from all puzzles
    pub fn get_all_scores(&self) -> Result<HashMap<u32, Vec<models::GameScore>>> {
        let scores_ns = self
            .open_tree("scores")
            .context("Opening DB namespace `scores` failed")?;
        let mut all_scores = HashMap::new();
        for puzzle in scores_ns.iter() {
            if let Ok(p) = puzzle {
                let uid =
                    p.0.to_vec()
                        .try_into()
                        .map(u32::from_ne_bytes)
                        .map_err(|_| anyhow!("Conversion to [u8; 4] failed"))?;
                let scores: Vec<models::GameScore> = serde_json::from_slice(&p.1)?;
                all_scores.insert(uid, scores);
            } else {
                error!("Couldn't read DB entry!");
            }
        }
        Ok(all_scores)
    }

    /// Sets scores for all puzzles; overwrites existing scores
    pub fn set_all_scores(
        &mut self,
        new_scores: HashMap<u32, Vec<models::GameScore>>,
    ) -> Result<()> {
        let scores_ns = self
            .open_tree("scores")
            .context("Opening DB namespace `scores` failed")?;
        scores_ns.clear()?;
        for (puzzle_id, puzzle_scores) in new_scores.into_iter() {
            let db_key = puzzle_id.to_ne_bytes();
            let db_value =
                serde_json::to_vec(&puzzle_scores).context("Failed to serialise game scores")?;
            scores_ns.insert(db_key, db_value)?;
        }
        Ok(())
    }

    /// Sets scores for all puzzles; existing scores are merged if possible
    pub fn update_all_scores(
        &mut self,
        new_scores: HashMap<u32, Vec<models::GameScore>>,
    ) -> Result<()> {
        let all_scores_in_db = self.get_all_scores()?;
        let scores_ns = self
            .open_tree("scores")
            .context("Opening DB namespace `scores` failed")?;
        for (puzzle_id, puzzle_scores) in new_scores.into_iter() {
            let db_key = puzzle_id.to_ne_bytes();
            if all_scores_in_db.contains_key(&puzzle_id) {
                let mut puzzle_scores_in_db: HashSet<models::GameScore> =
                    HashSet::from_iter(all_scores_in_db.get(&puzzle_id).unwrap().iter().cloned());
                puzzle_scores_in_db.extend(puzzle_scores);
                let db_key = puzzle_id.to_ne_bytes();
                let db_value = serde_json::to_vec(&puzzle_scores_in_db)
                    .context("Failed to serialise game scores")?;
                scores_ns.remove(db_key)?;
                scores_ns.insert(db_key, db_value)?;
            } else {
                let db_value = serde_json::to_vec(&puzzle_scores)
                    .context("Failed to serialise game scores")?;
                scores_ns.insert(db_key, db_value)?;
            }
        }
        Ok(())
    }

    /// Returns all puzzle ids
    pub fn get_all_puzzles(&self) -> Result<Vec<u32>> {
        let scores_ns = self
            .open_tree("scores")
            .context("Opening DB namespace `scores` failed")?;
        let mut res = vec![];
        for score in &scores_ns {
            res.push(ivec_as_u32(score?.0)?);
        }
        Ok(res)
    }

    /// Returns all puzzle ids with respective number of scores
    pub fn get_all_puzzles_with_score_no(&self) -> Result<HashMap<u32, usize>> {
        let mut res = HashMap::new();
        for puzzle_id in self.get_all_puzzles()? {
            let scores = self.get_scores(puzzle_id)?.map_or(0, |s| s.len());
            res.insert(puzzle_id, scores);
        }
        Ok(res)
    }

    /// Deletes all scores of a puzzle with its id
    pub fn delete_scores(&self, puzzle_id: u32) -> Result<Option<()>> {
        debug!(
            "Deleting all scores of puzzle {} from DB namespace `scores`",
            puzzle_id
        );
        let scores_ns = self
            .open_tree("scores")
            .context("Opening DB namespace `scores` failed")?;
        if scores_ns
            .contains_key(puzzle_id.to_ne_bytes())
            .context("Checking for DB key failed")?
        {
            scores_ns
                .remove(puzzle_id.to_ne_bytes())
                .with_context(|| format!("Deleting {puzzle_id} failed"))?;
            Ok(Some(()))
        } else {
            Ok(None)
        }
    }

    /// Deletes a certain entry from a game's scores
    pub fn delete_score(&mut self, puzzle_id: u32, score_id: u32) -> Result<Option<()>> {
        debug!(
            "Deleting score with id {} of puzzle {} from DB namespace `scores`",
            score_id, puzzle_id
        );
        let scores = self.get_scores(puzzle_id)?;
        if let Some(s) = scores {
            let updated_scores = s
                .into_iter()
                .filter(|score| score.uid != score_id)
                .collect::<Vec<models::GameScore>>();
            self.insert(
                puzzle_id.to_ne_bytes(),
                serde_json::to_vec(&updated_scores).context("Failed to serialise game score")?,
            )
            .map(|_| Some(()))
            .map_err(std::convert::Into::into)
        } else {
            Ok(None)
        }
    }

    /// Returns rank of game result
    pub fn get_rank(&self, puzzle_id: u32, score_id: u32) -> Result<Option<usize>> {
        let scores = self.get_scores(puzzle_id)?;
        if let Some(ra) = scores {
            let mut rank = 0;
            for (pos, r) in ra.iter().enumerate() {
                if r.uid == score_id {
                    rank = pos + 1;
                    break;
                }
            }
            Ok(Some(rank))
        } else {
            Ok(None)
        }
    }

    /// Adds result to scores and returns rank of result
    pub fn add_score(&mut self, puzzle_uid: u32, res: models::GameScore) -> Result<Option<usize>> {
        let score_uid = res.uid;
        let scores = self
            .get_scores(puzzle_uid)
            .with_context(|| format!("Fetching scores for puzzle `{puzzle_uid}` failed"))?;
        debug!(
            "Adding score with uid {} for puzzle {} to DB namespace `scores`",
            res.uid, puzzle_uid
        );
        let updated_scores = if let Some(mut r) = scores {
            r.push(res);
            r.sort_by(|x, y| x.secs.cmp(&y.secs));
            r
        } else {
            vec![res]
        };
        let scores_ns = self
            .0
            .open_tree("scores")
            .context("Opening DB namespace `scores` failed")?;
        scores_ns.insert(
            puzzle_uid.to_ne_bytes(),
            serde_json::to_vec(&updated_scores).context("Failed to serialise game score")?,
        )?;
        self.get_rank(puzzle_uid, score_uid)
    }
}

impl Deref for Db {
    type Target = sled::Db;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

/// Returns an [`IVec`] as a [`u32`] representation
fn ivec_as_u32(ivec: IVec) -> Result<u32> {
    let four_bytes: [u8; 4] = ivec.as_ref().try_into()?;
    Ok(u32::from_ne_bytes(four_bytes))
}
