//! # Puzzle App: Backend
//!
//! The Puzzle App is a web based application for playing jigsaw puzzles. Its
//! hallmark feature is the support of [IIIF manifests](https://iiif.io/api/presentation/3.0/) as a way of importing new
//! images and their respective metadata.
//!
//! This component (the backend) provides the server which generates the puzzles
//! and provides them to the
//! [frontend](https://gitlab.switch.ch/ub-unibas/puzzle-app/frontend). The SVG
//! paths used for the jigsaw templates are provided by the [puzzle-paths library](https://gitlab.switch.ch/ub-unibas/puzzle-app/puzzle-paths),
//!  which is part of this project.
//!
//! More precisely, the backend performs the following tasks:
//! - Importing images either from a designated import directory or via IIIF manifests (local or
//! remote)
//! - Creating copies from the images for catering different dimenions and building
//! puzzles from these copies
//! - Acting as a file server to serve the puzzles to a frontend
//! - Providing a API to store and retrieve ranking lists for puzzles
//!
//!
//! ## Requirements, setup and caveats
//!
//! See [README](https://gitlab.switch.ch/ub-unibas/puzzle-app/backend/-/blob/main/README.md)
//!
//! ## Running
//!
//! ```sh
//! ./image_server <path_to_config> <options>
//! ```
//!
//!
//! ### Configuration
//!
//! Settings can be provided via command line arguments, environment variables or a
//! configuration file, the formers taking precedence over the latters. If no path
//! to a configuration file is define via command line arguments (`-c` /
//! `--config-file`) or environment variables (`CONFIG_FILE`), the configuration
//! file is expected to be found at `./configs/main.toml`. If a setting is not
//! defined by either of the three possible variants, a default value is used. See
//! these default values along with information on each setting by running
//! `./puzzle_server -h`. If you prefer to configure the application by file, you
//! can find a template in
//! [`puzzle_server/configs/main.example.toml`](./puzzle_server/configs/main.example.toml).
//!
//!
//! ## Usage
//!
//! ### Puzzle server's file structure
//!
//! see module-level description in [`file_server`]
//!
//!
//! #### index.json
//!
//! The `index.json` files found in the folder structures are intended to be consumed by a frontend application and provides the information needed to retrieve the puzzles. There are index files on three levels:
//!
//! - The top-level `index.json` (`puzzles/index.json`) provides information on all
//!   images. It can e.g. be used to create an overview page.
//! - The `index.json` in an image folder (`puzzles/<image_id>/index.json`)
//!   contains links to all puzzles of the image as well as metadata on the image.
//! - The `index.json` in the pieces's folder
//!   (`puzzles/<image_id>/<puzzle>/<geometry_id>/index.json`) comprises the links
//! to all pieces of the puzzle. Furthermore, it contains the same metadata as the
//! image's `index.json`.

#![warn(clippy::all, clippy::pedantic, clippy::nursery, clippy::cargo)]

mod api;
mod config;
mod custom_manifest;
mod db;
mod file_server;
mod http_client;
mod iiif;
mod image;
mod manifest_import;
mod puzzle;

use anyhow::{bail, Context, Result};
use api::ApiDoc;
use axum::routing::get_service;
use axum::Router;
use config::{Config, ImportMode};
use db::Db;
use http_client::HttpClient;
use hyper::Method;
use log::{error, info};
use std::path::Path;
use std::process::{Command, Stdio};
use std::sync::Arc;
use tokio::net::TcpListener;
use tokio::sync::Mutex;
use tower_http::cors::{Any, CorsLayer};
use tower_http::services::{ServeDir, ServeFile};
use utoipa::OpenApi;
use utoipa_swagger_ui::SwaggerUi;

/// Contains the information needed by the handlers
#[derive(Clone)]
struct AppState {
    /// App configuration
    config: Config,
    /// Pointer to internal database
    db: Arc<Mutex<Db>>,
    /// Http client
    http_client: Arc<HttpClient>,
}

#[tokio::main]
async fn main() -> Result<()> {
    if !has_imagemagick() {
        bail!("`magick` command not found or not executable. Make sure that ImageMagick >= 7.0 is installed");
    };

    env_logger::init();
    let config = config::read_settings().context("Reading settings failed")?;

    image::create_import_folder(&config.import_dir).context("Creating import folder failed")?;
    puzzle::create_puzzles_folder(&config.puzzle_dir).context("Creating puzzles folder failed")?;

    let db = Arc::new(Mutex::new(
        Db::open(&config.db_dir).context("Opening database failed")?,
    ));
    let client = Arc::new(HttpClient::new().context("Creating HTTP client failed")?);

    if config.import_mode == ImportMode::Reset {
        info!("Deleting existing puzzles because import mode is set to `reset`");
        db.lock()
            .await
            .delete_all_manifests()
            .context("Deleting all manifests in DB failed")?;
        file_server::remove_all_puzzles(&config.puzzle_dir)
            .with_context(|| format!("Removing all puzzles in {} failed", &config.puzzle_dir))?;
    }

    info!("Creating puzzles based on manifests defined in configuration");

    create_puzzles(&config, db.clone(), client.clone())
        .await
        .context("Initial import failed")?;

    info!("Starting server on {}", &config.host);

    start_server(&config, db.clone(), client.clone())
        .await
        .context("Starting server failed")?;

    Ok(())
}

/// Primitive check if `magick` command is available on OS
fn has_imagemagick() -> bool {
    Command::new("magick")
        .stdout(Stdio::null())
        .stderr(Stdio::null())
        .spawn()
        .is_ok()
}

/// Create puzzles from images found in manifests as they are linked in the configuration
async fn create_puzzles(
    config: &Config,
    db: Arc<Mutex<Db>>,
    client: Arc<HttpClient>,
) -> Result<()> {
    match config.import_mode {
        ImportMode::Off => info!("Skipping import step because reimport set to off"),
        _ if config.manifests.is_empty() => info!("No manifests in configuration defined"),
        _ => {
            // force, reset, incremental
            let mut manifest_uris = manifest_import::expand_manifest_list(&config.manifests);
            info!(
                "Attempting to import images from {} manifest files",
                manifest_uris.len()
            );
            if config.import_mode == ImportMode::Force
                || config.import_mode == ImportMode::Incremental
            {
                let manifests_in_db = db
                    .lock()
                    .await
                    .get_manifests()?
                    .into_iter()
                    .map(|m| m.uri)
                    .collect::<Vec<String>>();
                manifest_uris.extend(manifests_in_db);
            }
            let mut new_manifests = vec![];
            for manifest_uri in &manifest_uris {
                info!("Processing manifest {manifest_uri}");
                if config.import_mode == ImportMode::Force
                    || config.import_mode == ImportMode::Reset
                    || !db.lock().await.manifest_import_successful(manifest_uri)?
                {
                    match manifest_import::get_images_from_manifest(
                        manifest_uri,
                        db.clone(),
                        client.clone(),
                        &config.import_dir,
                        config.import_mode == ImportMode::Force,
                    )
                    .await
                    {
                        Err(import_err) => {
                            error!("Manifest import failed: {}", import_err);
                        }
                        Ok(Some(m)) => {
                            new_manifests.push(m);
                        }
                        Ok(None) => {}
                    }
                    info!("Manifest {manifest_uri} processed");
                } else {
                    info!("Ignore manifest {manifest_uri} because the manifest already has been imported without any errors and ImportMode is not set to `force`");
                }
            }
            info!("Creating jigsaw puzzles from image files");
            puzzle::create_puzzles_from_manifests(config, db, &new_manifests)
                .await
                .context("Ingesting files failed")?;
            info!("Indexing done");
        }
    }
    Ok(())
}

/// Create routes and start server
async fn start_server(config: &Config, db: Arc<Mutex<Db>>, client: Arc<HttpClient>) -> Result<()> {
    let frontend = if let Some(frontend_dir) = &config.frontend_dir {
        let index_html = format!("{frontend_dir}/index.html");
        let not_found_page = if Path::new(&index_html).exists() {
            index_html
        } else {
            "./404.html".to_string()
        };
        get_service(ServeDir::new(frontend_dir).not_found_service(ServeFile::new(not_found_page)))
    } else {
        get_service(ServeFile::new("./404.html"))
    };

    let app_state = AppState {
        config: config.clone(),
        db: db.clone(),
        http_client: client.clone(),
    };

    let cors = CorsLayer::new()
        .allow_methods([Method::GET, Method::POST, Method::DELETE])
        // allow requests from any origin
        .allow_headers(Any)
        .allow_origin(Any);

    let app = Router::new()
        .nest("/puzzles", file_server::puzzle_files_route())
        .nest("/api", api::routes(app_state.clone()))
        .merge(SwaggerUi::new("/api").url("/api/openapi.json", ApiDoc::openapi()))
        .nest_service("/", frontend)
        .layer(cors)
        .with_state(app_state);

    let tcp_listener = TcpListener::bind(config.host)
        .await
        .context("Setting up TcpListener failed")?;
    axum::serve(tcp_listener, app)
        .await
        .context("Starting server failed")?;
    Ok(())
}
