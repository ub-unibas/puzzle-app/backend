//! Wrapper around [`Client`] to facilitate downloading remote resources
//!
//! This module provides a utility method to download remote resources (images, manifests)

use std::{str, str::FromStr};

use anyhow::{bail, Context, Result};
use http_body_util::{Collected, Full};
use hyper::{body::Bytes, header, Uri};
use hyper_rustls::{HttpsConnector, HttpsConnectorBuilder};
use hyper_util::{
    client::legacy::{connect::HttpConnector, Client},
    rt::TokioExecutor,
};
use url::Url;

/// Wraps a [`Client`] and provides a method to download remote resources (images, manifests)
pub struct HttpClient(Client<HttpsConnector<HttpConnector>, Full<Bytes>>);

/// Wrapper around a [`Client`]
impl HttpClient {
    /// Build a new [`HttpClient`]
    pub(crate) fn new() -> Result<Self> {
        let https_connector = HttpsConnectorBuilder::new()
            .with_native_roots()?
            .https_or_http()
            .enable_http1()
            .build();
        Ok(Self(
            Client::builder(TokioExecutor::new()).build::<_, Full<Bytes>>(https_connector),
        ))
    }

    /// Returns remote resource
    pub(crate) async fn download_resource(&self, uri: &str) -> Result<Collected<Bytes>> {
        let mut temp_uri = Url::parse(uri).with_context(|| format!("Couldn't parse uri {uri}"))?;
        loop {
            let uri = if let Ok(u) = Uri::from_str(temp_uri.as_str())
                .with_context(|| format!("Parsing {temp_uri} as uri failed"))
            {
                u
            } else {
                bail!("Parsing of URI {} failed", temp_uri)
            };
            let res = if let Ok(r) = self.0.get(uri.clone()).await.context("GET request failed") {
                r
            } else {
                bail!("GET request to {} failed", temp_uri)
            };
            let status = &res.status();
            if status.is_client_error() || status.is_server_error() {
                let buf = http_body_util::BodyExt::collect(res).await?;
                let bytes = buf.to_bytes();
                if bytes.is_empty() {
                    bail!("On requesting {}, HTTP error {} returned", uri, status)
                }
                bail!(
                    "On requesting {}, HTTP error {} returned: {}",
                    uri,
                    status,
                    std::str::from_utf8(&bytes).unwrap_or("")
                )
            }
            match res.headers().get(header::LOCATION) {
                Some(loc) => match loc.to_str() {
                    Ok(l) if l.starts_with("http") => {
                        if let Ok(u) = Url::parse(l) {
                            temp_uri = u;
                        } else {
                            bail!("Parsing of URI {} failed", l)
                        }
                    }
                    Ok(l) => {
                        temp_uri = if let Ok(u) = temp_uri.join(l) {
                            u
                        } else {
                            bail!("Merging of URL failed!")
                        }
                    }
                    Err(_) => bail!("Can't extract str from location header"),
                },
                None => {
                    return http_body_util::BodyExt::collect(res)
                        .await
                        .context("Reading body of response failed")
                }
            }
        }
    }
}
