//! Creating puzzles from images
//!
//! This modules provides the functionalities to create puzzles from provided images. It relies
//! heavily on [ImageMagick](https://imagemagick.org/) to perform this task, so it is mandatory
//! that a recent version of the tool (>= 7.0) is available.

use crate::config::{Config, ImportMode};
use crate::db::models as db_models;
use crate::db::{self, Db};
use crate::file_server;
use crate::image::{get_image_metadata, models::ImageMetadata};

use anyhow::{anyhow, bail, Context, Result};
use log::{debug, error, warn};
use once_cell::sync::Lazy;
use regex::Regex;
use serde::{Deserialize, Serialize};
use tokio::sync::Mutex;

use std::fs;
use std::path::{Path, PathBuf};
use std::sync::Arc;
use std::{process, vec};

/// Provides information on an image
#[derive(Default, Serialize, Clone)]
pub struct ImageInfo {
    /// ID of image
    pub uid: u32,
    /// [`ImageMetadata`] of image
    pub metadata: ImageMetadata,
    /// Path to original file
    pub original_file_path: String,
    /// List of thumbnails (as [`ResizedImage`]s)
    pub thumbnails: Vec<ResizedImage>,
    /// List of image's puzzles (as [`PuzzleInfo`]s)
    pub puzzles: Vec<PuzzleInfo>,
}

impl ImageInfo {
    /// Converts resized images' and thumbnails' absolute paths to relative URL paths
    pub fn make_paths_relative(&self, file_path: &str) -> Self {
        Self {
            thumbnails: self
                .thumbnails
                .iter()
                .map(|ri| {
                    ResizedImage::new(
                        &ri.image_path
                            .replace(file_path.trim_end_matches('/'), "puzzles"),
                        ri.dimensions,
                    )
                })
                .collect::<Vec<ResizedImage>>(),
            puzzles: self
                .puzzles
                .iter()
                .map(|puzzle| puzzle.make_paths_relative(file_path))
                .collect(),
            ..self.clone()
        }
    }

    /// Adds a new resized image to the available puzzle dimensions
    pub fn add_resized_image(&mut self, mut puzzle: PuzzleInfo, resized_image: &ResizedImage) {
        if let Some(p) = self.puzzles.iter_mut().find(|p| p.size == puzzle.size) {
            p.resolutions.push(resized_image.clone());
        } else {
            puzzle.resolutions.push(resized_image.clone());
            self.puzzles.push(puzzle);
        }
    }
}

/// Provides information on a puzzle of an image
#[derive(Default, Deserialize, Serialize, Clone)]
pub struct PuzzleInfo {
    /// Path to puzzle's root folder
    path: String,
    /// Puzzle's size as `<number_of_pieces_horizontally> * <number_of_pieces_vertically>`
    size: (usize, usize),
    /// List of available resolutions
    resolutions: Vec<ResizedImage>,
}

impl PuzzleInfo {
    /// Converts resized image's absolute path to relative URL path
    pub fn make_paths_relative(&self, file_path: &str) -> Self {
        Self {
            resolutions: self
                .resolutions
                .iter()
                .map(|re| re.make_paths_relative(file_path))
                .collect(),
            path: self
                .path
                .replace(file_path.trim_end_matches('/'), "puzzles"),
            ..self.clone()
        }
    }
}

/// Basic information on a resized copy of an image
#[derive(Serialize, Deserialize, Clone)]
pub struct ResizedImage {
    /// Unique id of resolution
    pub resolution_uid: usize,
    /// Path to resized image
    pub image_path: String,
    /// Width and height of resized image
    pub dimensions: (f32, f32),
}

impl ResizedImage {
    /// Creates new [`ResizedImage`] instance
    pub fn new(path: &str, resolution: (f32, f32)) -> Self {
        debug!("{}", path);
        Self {
            image_path: path.to_owned(),
            resolution_uid: Self::geometry_id(path).map(|id| id as usize).unwrap(),
            dimensions: resolution,
        }
    }

    /// Converts resized image's absolute path to relative URL path
    pub fn make_paths_relative(&self, file_path: &str) -> Self {
        Self {
            image_path: self
                .image_path
                .replace(file_path.trim_end_matches('/'), "puzzles"),
            ..self.clone()
        }
    }

    /// Returns a geometry id which is extracted from file name
    pub fn geometry_id(path: &str) -> Option<u32> {
        Path::new(path)
            .file_stem()
            .and_then(|os| os.to_str())
            .map(|s| s.replace("thumbnail_", ""))
            .and_then(|s| s.parse::<u32>().ok())
    }
}

impl From<ResizedImage> for db_models::ResizedImage {
    fn from(val: ResizedImage) -> Self {
        Self {
            dimension_uid: val.resolution_uid,
            image_path: val.image_path,
            dimension: val.dimensions,
        }
    }
}

impl From<db::models::ResizedImage> for ResizedImage {
    fn from(value: db_models::ResizedImage) -> Self {
        Self {
            resolution_uid: value.dimension_uid,
            image_path: value.image_path,
            dimensions: value.dimension,
        }
    }
}

/// Information on a single puzzle piece
#[derive(Serialize, Clone)]
pub struct PieceInfo {
    /// `<position on x-axis>_<position on y-axis>`
    pub id: String,
    /// Relative path to piece image
    pub file_path: String,
    /// SVG path of the piece's contours
    pub svg_path: String,
    /// Position of piece on x-axis
    pub x_axis_pos: usize,
    /// Position of piece on y-axis
    pub y_axis_pos: usize,
}

impl PieceInfo {
    /// Creates new [`PieceInfo`] instance
    pub fn new(file_path: String, svg_path: String, pos_in_axes: (usize, usize)) -> Self {
        let (x_axis_pos, y_axis_pos) = pos_in_axes;
        let id = format!("{x_axis_pos}_{y_axis_pos}");
        Self {
            id,
            file_path,
            svg_path,
            x_axis_pos,
            y_axis_pos,
        }
    }

    /// Converts  image's absolute path to relative URL path
    pub fn make_paths_relative(&self, file_path: &str) -> Self {
        Self {
            file_path: self
                .file_path
                .replace(file_path.trim_end_matches('/'), "puzzles"),
            ..self.clone()
        }
    }
}

/// Creates puzzles folder if none exists
pub fn create_puzzles_folder(path: &str) -> Result<()> {
    fs::create_dir_all(path).with_context(|| format!("Cannot create puzzles path {}", path))
}

/// Creates puzzles from manifests and updates root index.json
pub async fn create_puzzles_from_manifests(
    config: &Config,
    db: Arc<Mutex<Db>>,
    manifests: &[db_models::Manifest],
) -> Result<()> {
    for manifest in manifests {
        if let Err((e, image_path)) =
            create_puzzles_from_manifest(&manifest, config, db.clone()).await
        {
            error!(
                "Errors happened when creating puzzles for manifest {}: {}",
                manifest.uri, e
            );
            if let Some(ip) = image_path {
                std::fs::remove_dir_all(ip).unwrap();
            };
        }
    }
    let available_images = db.lock().await.get_all_available_images()?;
    file_server::image_index(available_images, &config.puzzle_dir)
        .context("Creating image index failed")?;
    Ok(())
}

/// Creates puzzles from a manifest
async fn create_puzzles_from_manifest(
    manifest: &db_models::Manifest,
    config: &Config,
    db: Arc<Mutex<Db>>,
) -> std::result::Result<Vec<ImageInfo>, (String, Option<String>)> {
    let mut image_infos = vec![];
    let mut import_errors = vec![];
    if manifest.status == db_models::ImportStatus::Pending
        || manifest.status.has_err()
        || config.import_mode == ImportMode::Force
    {
        let mut db = db.lock().await;
        db.update_manifest_import_status(manifest.uid, db_models::ImportStatus::Pending)
            .map_err(|e| (e.to_string(), None))?;
        let images_metadata = get_image_metadata(&manifest.body);
        let image_metadata = |uid| images_metadata.iter().find(|im| im.uid == uid);
        for image in &manifest.images {
            let image_path = puzzle_path(&config.puzzle_dir, image.uid, None, None, None)
                .with_context(|| {
                    format!(
                        "Concatenating path to image directory for image {} failed",
                        &image.original_path
                    )
                })
                .map_err(|e| (e.to_string(), None))?;
            recreate_dir(&image_path).map_err(|e| (e.to_string(), Some(image_path.clone())))?;
            let thumbnails = create_thumbnails(image, config)
                .with_context(|| {
                    format!(
                        "Thumbnails creation for image file {} failed",
                        &image.original_path
                    )
                })
                .map_err(|e| (e.to_string(), Some(image_path.clone())))?;
            db.add_thumbnails(
                manifest.uid,
                image.uid,
                thumbnails.iter().map(|t| t.clone().into()).collect(),
            )
            .map_err(|e| (e.to_string(), Some(image_path.clone())))?;
            match create_resized_copies(image, config) {
                Ok(resized_copies) => {
                    let mut image_info = ImageInfo {
                        uid: image.uid,
                        metadata: image_metadata(image.uid).unwrap().clone(),
                        original_file_path: image.original_path.clone(),
                        thumbnails,
                        ..ImageInfo::default()
                    };
                    db.add_resized_image(
                        manifest.uid,
                        image.uid,
                        &resized_copies
                            .iter()
                            .map(|rc| {
                                let resized_image: db_models::ResizedImage = rc.clone().into();
                                resized_image
                            })
                            .collect(),
                    )
                    .map_err(|e| (e.to_string(), Some(image_path.clone())))?;
                    for resized_copy in resized_copies {
                        let puzzles = create_puzzles(&image_info, &resized_copy, config)
                            .context("Creation of puzzles failed")
                            .map_err(|e| (e.to_string(), Some(image_path.clone())))?;
                        for puzzle in puzzles {
                            image_info.add_resized_image(puzzle, &resized_copy);
                        }
                    }
                    file_server::puzzles_index(&image_info, &config.puzzle_dir)
                        .context("Creating index file with puzzles in image folder failed")
                        .map_err(|e| (e.to_string(), Some(image_path.clone())))?;

                    image_infos.push(image_info);
                }
                Err(e) => {
                    let err_msg = format!(
                        "Copying image file {} failed: {:?}",
                        &image.original_path, e
                    );
                    warn!("{}", &err_msg);
                    import_errors.push(err_msg);
                }
            }
            fs::remove_file(&image.original_path).map_err(|e| {
                (
                    format!(
                        "Removing file {} in import folder failed: {}",
                        &image.original_path, e
                    ),
                    Some(image_path),
                )
            })?;
        }
        db.update_manifest_import_status(
            manifest.uid,
            if import_errors.is_empty() {
                db_models::ImportStatus::Finished
            } else {
                db_models::ImportStatus::Error(import_errors.join("; "))
            },
        )
        .map_err(|e| (e.to_string(), None))?;
    } else {
        debug!("Don't create puzzles from manifest {} as its puzzles have already been created successfully and ImportMode is not set to `force`", &manifest.uri);
    }
    Ok(image_infos)
}

/// Creates resized copies of original images as defined in the
/// [`Config#structfield.puzzle_geometries`]
#[allow(clippy::type_complexity)]
fn create_resized_copies(
    image_db_entry: &db_models::Image,
    config: &Config,
) -> Result<Vec<ResizedImage>> {
    let geometries = &config.puzzle_geometries;
    let mut resized_images = vec![];
    for geometry in geometries {
        let geometry_id = cityhasher::hash::<u32>(&geometry);
        let orig_file_name = if let Some(orig_file_name) = Path::new(&image_db_entry.original_path)
            .extension()
            .and_then(|ext| ext.to_str())
            .map(|ext| format!("{geometry_id}.{ext}"))
        {
            orig_file_name
        } else {
            error!(
                "Extracting extension from image's original file path ({}) failed",
                &image_db_entry.original_path
            );
            continue;
        };
        let resized_file_path = puzzle_path(
            &config.puzzle_dir,
            image_db_entry.uid,
            None,
            None,
            Some(&orig_file_name),
        )
        .with_context(|| {
            format!(
                "Concatenating path to resized images for image {} failed",
                &image_db_entry.original_path
            )
        })?;
        match create_resized_copy(
            &image_db_entry.original_path,
            &resized_file_path,
            geometry,
            config,
        ) {
            Ok(resized_image) => {
                resized_images.push(resized_image);
            }
            Err(e) => {
                error!(
                    "Resize of file {} failed: {}",
                    &image_db_entry.original_path, e
                );
            }
        }
    }
    Ok(resized_images)
}

/// Creates thumbnails of original image as defined in the [`Config#structfield.thumbnail_geometries`]
fn create_thumbnails(image_file: &db_models::Image, config: &Config) -> Result<Vec<ResizedImage>> {
    let mut thumbnails = vec![];
    let image_ext = Path::new(&image_file.original_path)
        .extension()
        .and_then(|e| e.to_str())
        .ok_or(anyhow!(
            "Image file has no extension. This should not happen!"
        ))
        .with_context(|| {
            format!(
                "Getting extension for image {} failed",
                &image_file.original_path
            )
        })?;
    for geometry in &config.thumbnail_geometries {
        let geometry_id = cityhasher::hash::<u32>(&geometry);
        let thumbnail_path = puzzle_path(
            &config.puzzle_dir,
            image_file.uid,
            None,
            None,
            Some(&format!("thumbnail_{geometry_id}.{image_ext}")),
        )
        .with_context(|| {
            format!(
                "Concatenating path to thumbnail image with geometry {} for image {} failed",
                geometry, &image_file.original_path
            )
        })?;
        debug!("Creating thumbnail {}", &thumbnail_path);
        match create_resized_copy(&image_file.original_path, &thumbnail_path, geometry, config) {
            Ok(resized_image) => {
                debug!("Creation of thumbnail {} successful", &thumbnail_path);
                thumbnails.push(resized_image);
            }
            Err(e) => {
                warn!("Thumbnail creation {} failed: {}", &thumbnail_path, e);
            }
        }
    }
    Ok(thumbnails)
}

/// Creates a resized copy of an original image
fn create_resized_copy(
    in_path: &str,
    out_path: &str,
    geometry: &str,
    config: &Config,
) -> Result<ResizedImage> {
    debug!(
        "Copy image file {} to {} and resize it to a geometry of {}",
        &in_path, &out_path, geometry,
    );
    let cmd = create_resize_cmd(&config.resize_args, in_path, out_path, geometry);
    let resolution = if let Some(args) = shlex::split(&cmd) {
        let output = process::Command::new("magick")
            .args(args)
            .output()
            .context("Execution of `magick` command failed")?;
        if !output.status.success() {
            let stdout_msg = String::from_utf8(output.stdout)
                .context("Reading in stdout from executing `magick` command failed")?;
            let stderr_msg = String::from_utf8(output.stderr)
                .context("Reading in stderr from executing `magick` command failed")?;
            let msg = format!(
                "Error resizing image with `magick` (error code {})",
                output.status
            );
            error!("{}", &msg);
            debug!("STDOUT: {}", &stdout_msg);
            debug!("STDERR: {}", &stderr_msg);
        }
        get_dimensions(out_path)
            .with_context(|| format!("Getting height from newly created image {out_path} failed"))?
    } else {
        bail!("convert args building failed");
    };
    Ok(ResizedImage::new(out_path, resolution))
}

/// Extracts width and height of an image
fn get_dimensions(path: &str) -> Result<(f32, f32)> {
    let cmd_args = format!(r#"identify -format "%wx%h" {path}"#);
    if let Some(args) = shlex::split(&cmd_args) {
        let output = process::Command::new("magick")
            .args(args)
            .output()
            .context("Execution of `magick` command failed")?;
        if !output.status.success() {
            let stdout_msg = String::from_utf8(output.stdout.clone())
                .context("Reading in stdout from executing `magick` command failed")?;
            let stderr_msg = String::from_utf8(output.stderr)
                .context("Reading in stderr from executing `magick` command failed")?;
            let msg = format!(
                "Error getting image dimensions with `identify` (error code {})",
                output.status
            );
            error!("{}", &msg);
            debug!("STDOUT: {}", &stdout_msg);
            debug!("STDERR: {}", &stderr_msg);
        }
        let output = String::from_utf8(output.stdout)
            .context("Parsing of output from `magick` command failed")?;
        let width_height: [&str; 2] = output
            .split('x')
            .collect::<Vec<&str>>()
            .try_into()
            .map_err(|_| {
                anyhow!("Fetching widht and height from `magick identify` command failed.")
            })?;
        Ok((
            width_height[0]
                .parse::<f32>()
                .context("Parsing of width from `magick identify` command as f32 failed")?,
            width_height[1]
                .parse::<f32>()
                .context("Parsing of width from `magick identify` command as f32 failed")?,
        ))
    } else {
        Err(anyhow!("convert args building failed"))
    }
}

/// Creates puzzles of a resized image as defined in [`Config#structfield.prerendered_pieces`] and
/// returns a list of puzzle paths and the related number of pieces if creation was successful
fn create_puzzles(
    image_info: &ImageInfo,
    resized_image: &ResizedImage,
    config: &Config,
) -> Result<Vec<PuzzleInfo>> {
    let pieces = &config.puzzle_sizes;
    let mut out_paths = vec![];
    for p in pieces {
        let p = match p {
            crate::config::PuzzleSize::Total(t) => puzzle_paths::generate_columns_rows_numbers(
                resized_image.dimensions.0,
                resized_image.dimensions.1,
                *t,
            ),
            crate::config::PuzzleSize::RowsCols(row_cols) => *row_cols,
        };
        if piece_in_allowed_size_range(p, resized_image.dimensions, config) {
            let puzzle_path = puzzle_path(
                &config.puzzle_dir,
                image_info.uid,
                ResizedImage::geometry_id(&resized_image.image_path),
                Some(p),
                None,
            )
            .with_context(|| {
                format!(
                    "Concatenating path to pieces folder {}x{} for image {} failed",
                    p.0, p.1, &resized_image.image_path
                )
            })?;
            debug!("Create contours of puzzle pieces in {}", &puzzle_path);
            recreate_dir(&puzzle_path).with_context(|| {
                format!(
                    "Checking if puzzle in {} should be created failed",
                    &puzzle_path
                )
            })?;
            match create_puzzle(
                &puzzle_path,
                &resized_image.image_path,
                resized_image.dimensions,
                p,
                config,
            ) {
                Ok((pieces_paths, piece_width, piece_height)) => {
                    file_server::puzzle_index(
                        image_info,
                        &pieces_paths,
                        p,
                        piece_width,
                        piece_height,
                        resized_image.image_path.clone(),
                        &puzzle_path,
                        &config.puzzle_dir,
                    )
                    .with_context(|| {
                        format!(
                            "Creating index file with puzzle pieces for puzzle {} failed",
                            &puzzle_path
                        )
                    })?;
                    out_paths.push(PuzzleInfo {
                        path: puzzle_path,
                        size: p,
                        ..PuzzleInfo::default()
                    });
                }
                Err(e) => {
                    fs::remove_dir_all(&puzzle_path).with_context(|| {
                        format!(
                            "Removing file {:?} as part of a clean-up failed",
                            &puzzle_path
                        )
                    })?;

                    warn!(
                        "Creating of puzzle pieces in file {} failed: {}",
                        &resized_image.image_path, e
                    );
                    break;
                }
            }
        } else {
            debug!("Pieces are not in allowed size range. Skipping generation of puzzle");
        }
    }
    Ok(out_paths)
}

/// Creates one puzzle of a resized image
fn create_puzzle(
    puzzle_path: &str,
    in_file_path: &str,
    resolution: (f32, f32),
    pieces: (usize, usize),
    config: &Config,
) -> Result<(Vec<PieceInfo>, f32, f32)> {
    let mut pieces_info = vec![];
    let jigsaw_template = puzzle_paths::build_jigsaw_template(
        resolution.0,
        resolution.1,
        pieces.0,
        pieces.1,
        Some(config.tab_size),
        Some(config.jitter),
        Some(config.seed as usize),
    );
    for (counter, svg_path) in jigsaw_template.svg_paths.into_iter().enumerate() {
        let pos_in_axes = pos_in_axes(counter, pieces);
        let file_path = format!("{}/{}_{}.png", &puzzle_path, pos_in_axes.0, pos_in_axes.1,);
        let cmd = create_cut_out_cmd(
            &config.cut_out_args,
            &svg_path,
            in_file_path,
            &file_path,
            resolution.0 as usize / pieces.0,
        );
        if let Some(args) = shlex::split(&cmd) {
            let output = process::Command::new("magick")
                .args(args)
                .output()
                .context("Execution of `magick` command failed")?;
            if !output.status.success() {
                let stdout_msg = String::from_utf8(output.stdout)
                    .context("Reading in stdout from executing `magick` command failed")?;
                let stderr_msg = String::from_utf8(output.stderr)
                    .context("Reading in stderr from executing `magick` command failed")?;
                let msg = format!(
                    "Error cutting out puzzle pieces with `magick` (error code {})",
                    output.status
                );
                error!("{}", &msg);
                debug!("STDOUT: {}", &stdout_msg);
                debug!("STDERR: {}", &stderr_msg);
            } else {
                debug!("Puzzle piece {} successfully built", &file_path);
                let svg_path =
                    normalise_svg_path(svg_path).context("Normalising SVG path failed")?;
                pieces_info.push(PieceInfo::new(file_path, svg_path, pos_in_axes));
            }
        } else {
            bail!("convert args building failed");
        }
    }
    Ok((
        pieces_info,
        jigsaw_template.piece_dimensions.0,
        jigsaw_template.piece_dimensions.1,
    ))
}

/// Returns position of piece in puzzle grid
fn pos_in_axes(pos: usize, pieces: (usize, usize)) -> (usize, usize) {
    (pos % pieces.0, pos / pieces.0)
}

/// Builds a path in the puzzles folder tree
fn puzzle_path(
    root_dir: &str,
    uid: u32,
    geometry_id: Option<u32>,
    pieces: Option<(usize, usize)>,
    out_file_name: Option<&str>,
) -> Result<String> {
    let uid_as_str = &uid.to_string();
    let uid_dir = Path::new(uid_as_str)
        .file_stem()
        .ok_or(anyhow!("{} is no valid file name", uid))
        .with_context(|| format!("Extracting file stem from {uid} failed"))?;

    let mut path_buf = PathBuf::new();
    path_buf.push(root_dir);
    path_buf.push(uid_dir);

    if let Some(p) = pieces {
        let pieces_dir = format!("{}x{}", p.0, p.1);
        path_buf.push(&pieces_dir);
    }
    if let Some(r) = geometry_id {
        let geometry_id_dir = r.to_string();
        path_buf.push(&geometry_id_dir);
    }
    if let Some(f) = out_file_name {
        path_buf.push(f);
    }
    Ok(path_buf.to_string_lossy().to_string())
}

/// Creates directory. If the directory already exists, the directory is deleted recursively and newly created
fn recreate_dir(dir_path: &str) -> Result<()> {
    let p = Path::new(dir_path);
    if p.exists() && p.is_dir() {
        fs::remove_dir_all(p)
            .with_context(|| format!("Removing file {p:?} as part of a clean-up failed"))?;
    }
    fs::create_dir_all(p).with_context(|| format!("Creating directory {p:?} failed"))?;
    Ok(())
}

/// Creates the `ImageMagick` command for cutting out pieces from an image based on the template in
/// the [`Config#structfield.cut_out_args`]
fn create_cut_out_cmd(
    cmd_template: &str,
    svg_path: &str,
    in_file: &str,
    out_file: &str,
    piece_width: usize,
) -> String {
    let svg_path = format!("\"path '{svg_path}'\"");
    cmd_template
        .replace("{{input_file}}", in_file)
        .replace("{{svg_path}}", &svg_path)
        .replace("{{output_file}}", out_file)
        .replace("{{shadow_layers}}", &create_shadow(piece_width))
}

/// Adds a shadow to the puzzle piece
fn create_shadow(piece_width: usize) -> String {
    let mut x = 0;
    let mut y = 1;
    let mut shadow_layers =
        format!("( +clone -fill DarkSlateGrey -colorize 100% -repage +{x}+{y} ) ");
    x += 1;
    y += 1;
    shadow_layers.push_str(&format!("( +clone -repage +{x}+{y} ) "));
    for i in 1..(piece_width / 75) {
        y += 1;
        if i % 3 == 0 {
            x += 1;
        }
        shadow_layers.push_str(&format!(" ( +clone -repage +{x}+{y} )"));
    }
    shadow_layers
}

/// Creates the `ImageMagick` command for resizing images based on the template in the
/// [`Config#structfield.resize_args`]
fn create_resize_cmd(cmd_template: &str, in_file: &str, out_file: &str, geometry: &str) -> String {
    cmd_template
        .replace("{{input_file}}", in_file)
        .replace("{{geometry}}", geometry)
        .replace("{{output_file}}", out_file)
}

/// Normalises the SVG path to a starting point of 0,0
fn normalise_svg_path(path: String) -> Result<String> {
    let m: Lazy<Regex> = Lazy::new(|| Regex::new(r"M (?<x>[0-9.]+),(?<y>[0-9.]+)").unwrap());
    let l: Lazy<Regex> = Lazy::new(|| Regex::new(r"L (?<x>[0-9.]+),(?<y>[0-9.]+)").unwrap());
    let c: Lazy<Regex> = Lazy::new(|| {
        Regex::new(r"C (?<x1>[0-9.]+),(?<y1>[0-9.]+) (?<x2>[0-9.]+),(?<y2>[0-9.]+) (?<x3>[0-9.]+),(?<y3>[0-9.]+)").unwrap()
    });
    let (x_diff, y_diff) = if let Some(starting_point) = m.captures(&path) {
        if let (Some(x), Some(y)) = (starting_point.name("x"), starting_point.name("y")) {
            let x = x.as_str();
            let y = y.as_str();
            if x == "0" && y == "0" {
                return Ok(path);
            }
            (x.parse::<f32>().unwrap(), y.parse::<f32>().unwrap())
        } else {
            bail!("Coordinates for starting point not found!");
        }
    } else {
        bail!("Starting point not found in path!");
    };
    let parts = path.split("  ");
    let mut new_path = String::new();
    for part in parts {
        if m.is_match(part) {
            let x = get_f32("x", part, &m)
                .context("Fetching x coordinate from starting point failed")?;
            let y = get_f32("y", part, &m)
                .context("Fetching y coordinate from starting point failed")?;
            new_path.push_str(&format!(
                "M {},{}",
                puzzle_paths::round(x - x_diff),
                puzzle_paths::round(y - y_diff)
            ));
        } else if l.is_match(part) {
            let x = get_f32("x", part, &l).context("Fetching x coordinate from line failed")?;
            let y = get_f32("y", part, &l).context("Fetching y coordinate from line failed")?;
            new_path.push_str(&format!(
                "  L {},{}",
                puzzle_paths::round(x - x_diff),
                puzzle_paths::round(y - y_diff)
            ));
        } else if c.is_match(part) {
            let x1 = get_f32("x1", part, &c).context(
                "Fetching x coordinate from first control point in cubic Bézier curve failed",
            )?;
            let y1 = get_f32("y1", part, &c).context(
                "Fetching y coordinate from first control point in cubic Bézier curve failed",
            )?;
            let x2 = get_f32("x2", part, &c).context(
                "Fetching x coordinate from second control point in cubic Bézier curve failed",
            )?;
            let y2 = get_f32("y2", part, &c).context(
                "Fetching y coordinate from second control point in cubic Bézier curve failed",
            )?;
            let x3 = get_f32("x3", part, &c)
                .context("Fetching x coordinate from end point in cubic Bézier curve failed")?;
            let y3 = get_f32("y3", part, &c)
                .context("Fetching y coordinate from end point in cubic Bézier curve failed")?;

            new_path.push_str(&format!(
                "  C {},{} {},{} {},{}",
                puzzle_paths::round(x1 - x_diff),
                puzzle_paths::round(y1 - y_diff),
                puzzle_paths::round(x2 - x_diff),
                puzzle_paths::round(y2 - y_diff),
                puzzle_paths::round(x3 - x_diff),
                puzzle_paths::round(y3 - y_diff)
            ));
        } else {
            new_path.push_str("  ");
            new_path.push_str(part);
        }
    }
    Ok(new_path)
}

/// Checks if a single piece is not smaller or larger than the allowed size range
fn piece_in_allowed_size_range(
    pieces: (usize, usize),
    resolution: (f32, f32),
    config: &Config,
) -> bool {
    let x = (resolution.0 / pieces.0 as f32).round() as usize;
    let y = (resolution.1 / pieces.1 as f32).round() as usize;
    if let Some((min_x, min_y)) = config.minimal_piece_size {
        if min_x > x {
            warn!("Width of piece ({}px) is smaller than minimal allowed piece width ({}px), therefore no puzzle with this resolution and number of pieces is created", x, min_x);
            return false;
        }
        if min_y > y {
            warn!("Height of piece ({}px) is smaller than minimal allowed piece height ({}px), therefore no puzzle with this resolution and number of pieces is created", y, min_y);
            return false;
        }
    }
    if let Some((max_x, max_y)) = config.maximal_piece_size {
        if max_x < x {
            warn!("Width of piece ({}px) is larger than maximal allowed piece width ({}px), therefore no puzzle with this resolution and number of pieces is created", x, max_x);
            return false;
        }
        if max_y < y {
            warn!("Height of piece ({}px) is larger than maximal allowed piece height ({}px), therefore no puzzle with this resolution and number of pieces is created", y, max_y);
            return false;
        }
    }
    true
}

/// Extracts a named capture group from a string and converts it to [f32]
fn get_f32(group_name: &str, haystack: &str, re: &Lazy<Regex>) -> Option<f32> {
    re.captures(haystack)
        .and_then(|c| c.name(group_name))
        .map(|g| g.as_str())
        .and_then(|s| s.parse::<f32>().ok())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_puzzle_path() {
        let root_path = "puzzle_root";
        let orig_file_uid = 213;

        // Create image path
        let resolution = None;
        let pieces = None;
        let out_file_name = None;
        let res = puzzle_path(root_path, orig_file_uid, resolution, pieces, out_file_name);
        assert_eq!(res.unwrap(), String::from("puzzle_root/213"));

        // Create thumbnail path
        let resolution = None;
        let pieces = None;
        let out_file_name = Some("thumbnail.jpg");
        let res = puzzle_path(root_path, orig_file_uid, resolution, pieces, out_file_name);
        assert_eq!(res.unwrap(), String::from("puzzle_root/213/thumbnail.jpg"));

        // Create width dir path
        let geometry_id = None;
        let pieces = Some((2, 3));
        let out_file_name = None;
        let res = puzzle_path(root_path, orig_file_uid, geometry_id, pieces, out_file_name);
        assert_eq!(res.unwrap(), String::from("puzzle_root/213/2x3"));

        // Create resized image path
        let geometry = "100x50";
        let geometry_id = cityhasher::hash::<u32>(&geometry);
        let pieces = Some((2, 3));
        let out_file_name = Some("resized_image.jpg");
        let res = puzzle_path(
            root_path,
            orig_file_uid,
            Some(geometry_id),
            pieces,
            out_file_name,
        );
        assert_eq!(
            res.unwrap(),
            String::from("puzzle_root/213/2x3/3732705464/resized_image.jpg")
        );

        // Create pieces dir path
        let geometry = "100x50";
        let geometry_id = cityhasher::hash::<u32>(&geometry);
        let pieces = Some((2, 3));
        let out_file_name = None;
        let res = puzzle_path(
            root_path,
            orig_file_uid,
            Some(geometry_id),
            pieces,
            out_file_name,
        );
        assert_eq!(res.unwrap(), String::from("puzzle_root/213/2x3/3732705464"));
    }
}
