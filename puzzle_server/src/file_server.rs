//! Creating, managing and serving the files required by a frontend to provide a puzzle
//!
//! While starting up, the server builds the puzzles in the `puzzles` subdirectory and subsequently
//! exposes this directory via the webserver on `<host>/puzzles/`. The folder itself contains
//! - copies of the images catering the different predefined dimensions
//! - puzzle pieces for these dimensions and puzzle sizes
//! - thumbnails in predefined dimensions
//! - metadata files describing the images and related puzzles
//!
//! ## `puzzles` folder structure
//!
//! ```sh
//! .
//! ├── 2006011577                     # An image subdirectory
//! │   ├── 3736787576.jpg             # A resized copy of the original image
//! │   ├── 7x3                        # A puzzle from the image with a certain dimension
//! │   │   ├── 3736787576             # Folder containing the puzzle's pieces
//! │   │   │   ├── 0_0.png            # A puzzle piece
//! │   │   │   ├── <other_pieces>          
//! │   │   │   └── index.json         # Information on the puzzle's pieces
//! │   │   └── <other_geometry>
//! │   ├── index.json                 # Information on the image's puzzles
//! │   ├── thumbnail_2039956022.jpg   # A thumbnail of the image with a certain dimension
//! │   ├── <other_puzzle>
//! │   │   └── <...>
//! ├── <other_image>
//! └── index.json                     # Information on available images
//! ```

use std::{
    collections::HashMap,
    error::Error,
    fs::{self, File},
    path::{Path, PathBuf},
    sync::Arc,
};

use anyhow::{Context, Result};
use axum::{
    body::Body,
    extract::State,
    http::{Request, Response, Uri},
    response::Redirect,
    routing::get,
    Router,
};
use hyper::StatusCode;
use log::{debug, error, info, warn};
use serde::{Deserialize, Serialize};
use tokio::sync::Mutex;
use tower::ServiceExt;
use tower_http::services::{ServeDir, ServeFile};

use crate::{
    db::{self, Db},
    image::models::ImageMetadata,
    puzzle::{ImageInfo, PieceInfo, PuzzleInfo, ResizedImage},
    AppState,
};

/// Contains top-level information on all available images
#[derive(Serialize, Deserialize)]
struct ImagesIndex {
    /// List of available images as [`Image`]
    pub images: Vec<Image>,
    /// Number of all available images
    pub count: usize,
}

impl ImagesIndex {
    /// Builds [`ImagesIndex`] out of available information on images
    fn build(value: Vec<db::models::Image>, puzzle_root_file_path: &str) -> Result<Self> {
        let images = value
            .into_iter()
            .map(|i| Image {
                uid: i.uid,
                title: i.title.clone(),
                thumbnails: i
                    .thumbnails
                    .into_iter()
                    .map(std::convert::Into::into)
                    .map(|ri: ResizedImage| ri.make_paths_relative(puzzle_root_file_path))
                    .collect(),
            })
            .collect::<Vec<Image>>();
        let count = images.len();
        Ok(Self { images, count })
    }
}

/// Contains basic information on an image
#[derive(Serialize, Deserialize)]
struct Image {
    /// Image's uid
    uid: u32,
    /// Possibly multilingual title of image
    title: HashMap<String, Vec<String>>,
    /// List of attached thumbnails
    thumbnails: Vec<ResizedImage>,
}

/// Contains information on all puzzles of an image
#[derive(Serialize)]
struct PuzzlesIndex<'a> {
    /// Image's metadata
    pub metadata: &'a ImageMetadata,
    /// List of attached thumbnails
    pub thumbnails: &'a Vec<ResizedImage>,
    /// List of attached puzzles
    pub puzzles: Vec<PuzzleInfo>,
}

impl<'a> PuzzlesIndex<'a> {
    /// Builds a [`PuzzlesIndex`]
    fn build(image_info: &'a ImageInfo) -> PuzzlesIndex<'a> {
        Self {
            metadata: &image_info.metadata,
            thumbnails: &image_info.thumbnails,
            puzzles: image_info.puzzles.clone(),
        }
    }
}

/// Contains all information needed on a single puzzle
#[derive(Serialize)]
struct PiecesIndex<'a> {
    /// Puzzle's id
    pub id: u32,
    /// Metadata on image
    pub metadata: &'a ImageMetadata,
    /// Puzzle's pieces
    pub pieces: Vec<PieceInfo>,
    /// Number of pieces horizontally and vertically
    pub pieces_no: (usize, usize),
    /// Basic width of a single piece
    pub piece_width: f32,
    /// Basic height of a single piece
    pub piece_height: f32,
    /// Relative path to resized image file
    pub image_path: String,
}

impl<'a> PiecesIndex<'a> {
    /// Creates [`PiecesIndex`]
    fn build(
        puzzle_id: u32,
        image_info: &'a ImageInfo,
        pieces: &'a [PieceInfo],
        pieces_no: (usize, usize),
        piece_width: f32,
        piece_height: f32,
        image_path: String,
        puzzle_root_file_path: &str,
    ) -> Self {
        PiecesIndex {
            id: puzzle_id,
            metadata: &image_info.metadata,
            pieces: pieces
                .iter()
                .map(|pi| pi.make_paths_relative(puzzle_root_file_path))
                .collect::<Vec<PieceInfo>>(),
            pieces_no,
            piece_width,
            piece_height,
            image_path: image_path.replace(puzzle_root_file_path.trim_end_matches('/'), "puzzles"),
        }
    }
}

/// Creates file server route
pub fn puzzle_files_route() -> Router<AppState> {
    Router::new()
        .route("/", get(|| async { Redirect::to("puzzles/index.json") }))
        .route(
            "/*key",
            get(file_handler).fallback_service(ServeFile::new("puzzles/index.json")),
        )
}

/// Creates JSON index of all available images
pub fn image_index(images: Vec<db::models::Image>, puzzle_root_file_path: &str) -> Result<()> {
    let images_index = ImagesIndex::build(images, puzzle_root_file_path)?;
    let mut path_buf = PathBuf::new();
    path_buf.push(puzzle_root_file_path);
    path_buf.push("index.json");
    let f = File::create(path_buf.as_path())
        .with_context(|| format!("Cannot create or overwrite puzzle index file {path_buf:?}"))?;
    serde_json::to_writer(&f, &images_index)
        .with_context(|| format!("Serializing index struct to {path_buf:?} failed"))?;
    debug!(
        "Image index file in {:?} created or overwritten",
        &path_buf.as_os_str()
    );
    Ok(())
}

/// Creates JSON index of all puzzles of an image including metadata
pub fn puzzles_index(image: &ImageInfo, puzzle_root_file_path: &str) -> Result<()> {
    let rel_image_path = image.make_paths_relative(puzzle_root_file_path);
    let index = PuzzlesIndex::build(&rel_image_path);
    let mut path_buf = PathBuf::new();
    path_buf.push(puzzle_root_file_path);
    path_buf.push(image.uid.to_string());
    path_buf.push("index.json");
    let f = File::create(path_buf.as_path())
        .with_context(|| format!("Cannot create puzzle index file {path_buf:?}"))?;
    serde_json::to_writer(&f, &index)
        .with_context(|| format!("Serializing index struct to {path_buf:?} failed"))?;
    debug!("Puzzles index file in {:?} created", &path_buf.as_os_str());
    Ok(())
}

/// Creates JSON index of the puzzle's pieces
pub fn puzzle_index(
    image_info: &ImageInfo,
    pieces: &[PieceInfo],
    pieces_no: (usize, usize),
    piece_width: f32,
    piece_height: f32,
    resized_image_path: String,
    json_folder: &str,
    puzzle_root_file_path: &str,
) -> Result<()> {
    let index = PiecesIndex::build(
        cityhasher::hash::<u32>(json_folder),
        image_info,
        pieces,
        pieces_no,
        piece_width,
        piece_height,
        resized_image_path,
        puzzle_root_file_path,
    );
    let mut path_buf = PathBuf::new();
    path_buf.push(json_folder);
    path_buf.push("index.json");
    let f = File::create(path_buf.as_path())
        .with_context(|| format!("Cannot create puzzle index file {path_buf:?}"))?;
    serde_json::to_writer(&f, &index)
        .with_context(|| format!("Serializing index struct to {path_buf:?} failed"))?;
    debug!("Puzzle index file in {:?} created", &path_buf.as_os_str());
    Ok(())
}

/// Deletes `puzzles` directory and therefore all puzzles within
pub fn remove_all_puzzles(puzzle_path: &str) -> Result<()> {
    let path = Path::new(puzzle_path);
    fs::remove_dir_all(path)?;
    info!("Puzzle dir deleted");
    Ok(())
}

/// Deletes all puzzles belonging to a manifest
pub async fn remove_puzzles_from_manifest(
    db: Arc<Mutex<Db>>,
    puzzle_path: &str,
    manifest_id: u32,
) -> Result<Vec<u32>> {
    let lock = db.lock().await;
    if let Some(manifest) = lock.get_manifest(manifest_id)? {
        let image_uids: Vec<u32> = manifest.images.iter().map(|i| i.uid).collect();
        for image_uid in image_uids.iter() {
            let mut path_buf = PathBuf::new();
            path_buf.push(puzzle_path);
            path_buf.push(image_uid.to_string());
            fs::remove_dir_all(path_buf.as_path())?;
        }
        Ok(image_uids)
    } else {
        warn!("No manifest with uid {} found in DB", manifest_id);
        Ok(vec![])
    }
}

/// Handles requests for files in the `puzzles` folder
async fn file_handler(
    uri: Uri,
    State(state): State<AppState>,
) -> Result<Response<Body>, (StatusCode, String)> {
    let serve_dir = &state.config.puzzle_dir;
    match get_dir_content(uri.clone(), serve_dir).await {
        Ok(res) if res.status() == StatusCode::NOT_FOUND => {
            Err((StatusCode::NOT_FOUND, "File not found".to_string()))
        }
        Ok(res) => Ok(res),
        Err(e) => {
            error!("{}", e);
            Err((
                StatusCode::INTERNAL_SERVER_ERROR,
                "An internal error happened".to_string(),
            ))
        }
    }
}

/// Serves files in the `puzzles` folder
async fn get_dir_content(uri: Uri, serve_dir: &str) -> Result<Response<Body>, Box<dyn Error>> {
    let req = Request::builder().uri(uri).body(Body::empty())?;
    ServeDir::new(serve_dir)
        .oneshot(req)
        .await
        .map(|r| r.map(Body::new))
        .map_err(std::convert::Into::into)
}
