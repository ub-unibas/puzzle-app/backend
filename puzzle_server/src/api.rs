//! Providing routes and handlers for the app's RESTful API
//!
//! The API exposes endpoints to get information on manifests, available images and players'
//! scores. For the latters, it provides the means to modify and delete resources as well.

use std::collections::HashMap;

use crate::{db::models as db_models, file_server, manifest_import, puzzle, AppState};

use anyhow::anyhow;
use axum::{
    extract::{Path, Query, Request, State},
    http::StatusCode,
    middleware::{self, Next},
    response::{IntoResponse, Response},
    routing::{delete, get, post},
    Json, Router,
};
use chrono::Local;
use hyper::HeaderMap;
use log::{error, info};
use utoipa::{
    openapi::security::{ApiKey, ApiKeyValue, SecurityScheme},
    Modify, OpenApi, ToSchema,
};

pub mod models {
    //! Models used in the [`crate::api`] module

    use std::collections::HashMap;

    use serde::{Deserialize, Serialize};
    use utoipa::{IntoParams, ToSchema};

    /// Information on a single manifest
    #[derive(Deserialize, Serialize, ToSchema)]
    pub struct Manifest {
        /// Manifest's URI
        pub uri: String,
        /// Manifest's possibly multilingual title
        pub label: HashMap<String, Vec<String>>,
        /// Timestamp of last modification
        pub timestamp: i64,
        /// Stringified version of manifest's import status.
        pub status: String,
        /// IDs of comprised images
        pub images: Vec<u32>,
    }

    /// Information on an image
    #[derive(Deserialize, Serialize, ToSchema)]
    pub struct Image {
        /// Image's unique identifier
        pub uid: u32,
        /// Image's possibly multilingual title
        pub title: HashMap<String, Vec<String>>,
        /// Paths to resized images in the `puzzles/` directory
        pub images: Vec<String>,
        /// Paths to thumbnails in the `puzzles/` directory
        pub thumbnails: Vec<String>,
    }

    /// Game score used for exporting / importing scores
    #[derive(Deserialize, Serialize, ToSchema)]
    pub struct GameScore {
        /// Unique identifier
        #[schema(example = 27_322_231)]
        pub uid: u32,
        /// User name
        #[schema(example = "testuser")]
        pub name: String,
        /// Time in secs to finish the jigsaw puzzle
        #[schema(example = 100)]
        pub secs: usize,
        /// Timestamp of the game
        #[schema(example = 1_690_716_212)]
        pub timestamp: i64,
    }

    /// Single game score sent in response
    #[derive(Deserialize, Serialize, ToSchema)]
    #[serde(rename_all = "camelCase")]
    pub struct Rank {
        /// Unique identifier of game
        #[schema(example = 27_322_231)]
        pub uid: u32,
        /// Position in ranking
        #[schema(example = 2)]
        pub position: usize,
        /// User name
        #[schema(example = "testuser")]
        pub name: String,
        /// Time in seconds needed to play the game
        #[schema(example = 100)]
        pub secs: usize,
        /// Timestamp of game
        #[schema(example = 1_690_716_212)]
        pub date_time: i64,
    }

    /// New game score expected in request
    #[derive(Deserialize, ToSchema)]
    pub struct GameResult {
        /// Puzzle's unique identifier
        #[schema(example = 232_312_334)]
        pub puzzle: u32,
        /// User name
        #[schema(example = "testuser")]
        pub name: String,
        /// Time in seconds needed to play the game
        #[schema(example = 100)]
        pub secs: usize,
    }

    /// Query to post a manifest url
    #[derive(Deserialize, IntoParams)]
    pub struct ManifestUrlQuery {
        /// Manifest's url
        pub url: String,
    }

    /// Query to set import mode for scores
    #[derive(Deserialize, Serialize, IntoParams)]
    #[into_params(parameter_in = Query)]
    pub struct ImportScoresQuery {
        /// Should existing scores be overwriten
        #[serde(default)]
        pub overwrite: Option<bool>,
    }
}

/// OpenAPI base object
#[derive(OpenApi)]
#[openapi(
    paths(
        get_image,
        get_images,
        get_manifest,
        get_manifests,
        add_manifest,
        delete_manifest,
        get_puzzles,
        get_puzzles_with_scores,
        get_scores,
        add_score,
        delete_scores,
        delete_score,
        export_scores,
        import_scores,
    ),
    components(
        schemas(
            models::GameResult,
            models::GameScore,
            models::Image,
            models::Manifest,
            models::Rank,
        )
    ),
    modifiers(&SecurityAddon),
    tags(
        (name = "images", description = "Manage images"),
        (name = "manifests", description = "Manage manifests"),
        (name = "puzzles", description = "Manage puzzles"),
        (name = "scores", description = "Manage scores"),
    )
)]
pub struct ApiDoc;

/// OpenAPI's security layer
struct SecurityAddon;

impl Modify for SecurityAddon {
    fn modify(&self, openapi: &mut utoipa::openapi::OpenApi) {
        if let Some(components) = openapi.components.as_mut() {
            components.add_security_scheme(
                "api_key",
                SecurityScheme::ApiKey(ApiKey::Header(ApiKeyValue::new("X-API-Key"))),
            );
        }
    }
}

/// Operation errors
#[derive(ToSchema)]
pub enum AppError {
    /// ID / resource not found
    #[schema(example = "id = 4")]
    NotFound(String),
    /// An internal error happened
    InternalError(anyhow::Error),
}

impl IntoResponse for AppError {
    fn into_response(self) -> Response {
        match self {
            Self::NotFound(id) => {
                (StatusCode::NOT_FOUND, format!("{id} not found")).into_response()
            }
            Self::InternalError(e) => {
                error!("{}", &e);
                (
                    StatusCode::INTERNAL_SERVER_ERROR,
                    format!("Something went wrong: {e}"),
                )
                    .into_response()
            }
        }
    }
}

impl<E> From<E> for AppError
where
    E: Into<anyhow::Error>,
{
    fn from(err: E) -> Self {
        Self::InternalError(err.into())
    }
}

/// Internal server error
struct InternalError(anyhow::Error);

impl<E> From<E> for InternalError
where
    E: Into<anyhow::Error>,
{
    fn from(err: E) -> Self {
        Self(err.into())
    }
}

impl From<models::GameResult> for db_models::GameScore {
    fn from(val: models::GameResult) -> Self {
        let date_time = Local::now().timestamp_millis();
        Self {
            uid: cityhasher::hash::<u32>(format!("{}{}{}", val.name, val.secs, date_time)),
            name: val.name,
            secs: val.secs,
            timestamp: date_time,
        }
    }
}

impl From<db_models::GameScore> for models::GameScore {
    fn from(val: db_models::GameScore) -> Self {
        Self {
            uid: val.uid,
            name: val.name,
            secs: val.secs,
            timestamp: val.timestamp,
        }
    }
}

impl From<models::GameScore> for db_models::GameScore {
    fn from(val: models::GameScore) -> Self {
        Self {
            uid: val.uid,
            name: val.name,
            secs: val.secs,
            timestamp: val.timestamp,
        }
    }
}

impl From<db_models::Manifest> for models::Manifest {
    fn from(value: db_models::Manifest) -> Self {
        Self {
            uri: value.uri,
            label: value.label,
            timestamp: value.timestamp,
            status: match value.status {
                db_models::ImportStatus::Pending => "imported".to_string(),
                db_models::ImportStatus::Finished => "finished".to_string(),
                db_models::ImportStatus::Error(e) => format!("error: {e}"),
            },
            images: value.images.into_iter().map(|i| i.uid).collect(),
        }
    }
}

/// Does authorisation on request
async fn auth(
    State(state): State<AppState>,
    headers: HeaderMap,
    request: Request,
    next: Next,
) -> Result<Response, StatusCode> {
    if let Some(expected_api_key) = state.config.api_secret {
        if let Some(sent_api_key) = headers.get("X-API-Key") {
            if sent_api_key
                .to_str()
                .ok()
                .filter(|key| *key == expected_api_key)
                .is_some()
            {
                return Ok(next.run(request).await);
            }
        }
        return Err(StatusCode::UNAUTHORIZED);
    }
    Ok(next.run(request).await)
}

/// Create api routes
pub fn routes(state: AppState) -> Router<AppState> {
    Router::new()
        .route("/scores/:puzzle_id", delete(delete_scores))
        .route("/score/:puzzle_id/:score_id", delete(delete_score))
        .route("/scores", get(export_scores))
        .route("/scores", post(import_scores))
        .route("/manifest", post(add_manifest))
        .route("/manifest/:manifest_id", delete(delete_manifest))
        // Order is significant here: Routes above this layer have potentially restricted access
        .route_layer(middleware::from_fn_with_state(state, auth))
        .route("/scores/:puzzle_id", get(get_scores))
        .route("/score", post(add_score))
        .route("/puzzles", get(get_puzzles))
        .route("/puzzles/scores", get(get_puzzles_with_scores))
        .route("/manifest/:manifest_id", get(get_manifest))
        .route("/manifests", get(get_manifests))
        .route("/images", get(get_images))
        .route("/image/:image_id", get(get_image))
}

/// Exports scores of all puzzles
#[utoipa::path(
    get,
    path = "/api/scores", 
    security(
        ("api_key" = [])
    ),
    responses(
        (status = 200, description = "Scores of all puzzles", body = HashMap<u32, Vec<GameScore>>),
        (status = 500, description = "Internal server error")
    ),
    tag = "scores"

)]
pub async fn export_scores(
    State(state): State<AppState>,
) -> Result<Json<HashMap<u32, Vec<models::GameScore>>>, AppError> {
    let lock = state.db.lock().await;
    lock.get_all_scores()
        .map(|all_scores| {
            Json(
                all_scores
                    .into_iter()
                    .map(|(key, value)| (key, value.into_iter().map(|v| v.into()).collect()))
                    .collect(),
            )
        })
        .map_err(|err| AppError::InternalError(err))
}

/// Imports scores of all puzzles
#[utoipa::path(
    post,
    path = "/api/scores", 
    params(models::ImportScoresQuery),
    security(
        ("api_key" = [])
    ),
    request_body = HashMap<u32, Vec<GameScore>>,
    responses(
        (status = 200, description = "Scores successfully imported"),
        (status = 500, description = "Internal server error")
    ),
    tag = "scores"
)]
pub async fn import_scores(
    import_mode: Option<Query<models::ImportScoresQuery>>,
    State(state): State<AppState>,
    Json(new_scores): Json<HashMap<u32, Vec<models::GameScore>>>,
) -> Result<(), AppError> {
    let mut lock = state.db.lock().await;
    let new_scores = new_scores
        .into_iter()
        .map(|(key, value)| (key, value.into_iter().map(|v| v.into()).collect()))
        .collect();
    let overwrite = match import_mode {
        Some(Query(mode)) => mode.overwrite.unwrap_or(false),
        None => false,
    };
    if overwrite {
        lock.set_all_scores(new_scores)
            .map_err(|err| AppError::InternalError(err))
    } else {
        lock.update_all_scores(new_scores)
            .map_err(|err| AppError::InternalError(err))
    }
}

/// Returns scores for a puzzle
#[utoipa::path(
    get,
    path = "/api/scores/{puzzle_id}", 
    params(
        ("puzzle_id" = String, Path, description = "Puzzle id to get scores for")
    ),
    responses(
        (status = 200, description = "Puzzle scores", body = Vec<Rank>),
        (status = 404, description = "Puzzle was not found"),
        (status = 500, description = "Internal server error")
    ),
    tag = "scores"
)]
pub async fn get_scores(
    Path(puzzle_id): Path<String>,
    State(state): State<AppState>,
) -> Result<Json<Vec<models::Rank>>, AppError> {
    let lock = state.db.lock().await;
    let puzzle_id = if let Ok(id) = puzzle_id.parse::<u32>() {
        id
    } else {
        let err_msg = "Puzzle ID must be an integer!";
        error!("{}", err_msg);
        return Err(anyhow!(err_msg).into());
    };
    match lock.get_scores(puzzle_id) {
        Ok(Some(scores)) => Ok(Json(
            scores
                .into_iter()
                .enumerate()
                .map(|(pos, score)| models::Rank {
                    uid: score.uid,
                    position: pos + 1,
                    name: score.name,
                    secs: score.secs,
                    date_time: score.timestamp,
                })
                .collect::<Vec<models::Rank>>(),
        )),
        Ok(None) => Ok(Json(vec![])),
        Err(e) => {
            error!("{}", e);
            Err(anyhow!(e).into())
        }
    }
}

/// Removes all scores for a puzzle
#[utoipa::path(
    delete,
    path = "/api/scores/{puzzle_id}", 
    params(
        ("puzzle_id" = String, Path, description = "Puzzle id to remove scores for")
    ),
    security(
        ("api_key" = [])
    ),
    responses(
        (status = 200, description = "Puzzle scores removed"),
        (status = 404, description = "Puzzle was not found"),
        (status = 500, description = "Internal server error")
    ),
    tag = "scores"
)]
async fn delete_scores(
    Path(puzzle_id): Path<u32>,
    State(state): State<AppState>,
) -> Result<(), AppError> {
    let lock = state.db.lock().await;
    match lock.delete_scores(puzzle_id) {
        Ok(Some(())) => Ok(()),
        Ok(None) => Err(AppError::NotFound(format!("Puzzle with id {puzzle_id} "))),
        Err(e) => Err(AppError::InternalError(e)),
    }
}

/// Removes a single score entry
#[utoipa::path(
    delete,
    path = "/api/score/{puzzle_id}/{score_id}", 
    params(
        ("puzzle_id" = String, Path, description = "Puzzle id where score is saved"),
        ("score_id" = String, Path, description = "ID of score to be removed")
    ),
    security(
        ("api_key" = [])
    ),
    responses(
        (status = 200, description = "Puzzle scores removed"),
        (status = 404, description = "Puzzle was not found"),
        (status = 500, description = "Internal server error")
    ),
    tag = "scores"
)]
pub async fn delete_score(
    Path((puzzle_id, score_id)): Path<(String, u32)>,
    State(state): State<AppState>,
) -> Result<(), AppError> {
    let mut lock = state.db.lock().await;
    let puzzle_id = if let Ok(id) = puzzle_id.parse::<u32>() {
        id
    } else {
        let err_msg = "Puzzle ID must be an integer!";
        error!("{}", err_msg);
        return Err(anyhow!(err_msg).into());
    };
    match lock.delete_score(puzzle_id, score_id) {
        Ok(Some(())) => Ok(()),
        Ok(None) => Err(AppError::NotFound(format!(
            "Score {score_id} in puzzle {puzzle_id}"
        ))),
        Err(e) => Err(AppError::InternalError(e)),
    }
}

/// Returns all puzzle ids
#[utoipa::path(
    get,
    path = "/api/puzzles",
    responses(
        (status = 200, description = "All puzzle ids", body = Vec<u32>),
        (status = 500, description = "Internal server error")
    ),
    tag = "puzzles"
)]
pub async fn get_puzzles(State(state): State<AppState>) -> Result<Json<Vec<u32>>, AppError> {
    let lock = state.db.lock().await;
    match lock.get_all_puzzles() {
        Ok(puzzles) => Ok(Json(puzzles)),
        Err(e) => Err(AppError::InternalError(e)),
    }
}

/// Returns list of puzzles with related scores
#[utoipa::path(
    get,
    path = "/api/puzzles/scores",
    responses(
        (status = 200, description = "All puzzle ids with score numbers", body = Vec<HashMap<u32, usize>>),
        (status = 500, description = "Internal server error")
    ),
    tag = "puzzles"
)]
pub async fn get_puzzles_with_scores(
    State(state): State<AppState>,
) -> Result<Json<HashMap<u32, usize>>, AppError> {
    let lock = state.db.lock().await;
    match lock.get_all_puzzles_with_score_no() {
        Ok(puzzles) => Ok(Json(puzzles)),
        Err(e) => Err(AppError::InternalError(e)),
    }
}

/// Adds a new score and returns rank
#[utoipa::path(
    post,
    path = "/api/score", 
    request_body = GameResult,
    responses(
        (status = 200, description = "Game's rank", body = String),
        (status = 500, description = "Internal server error")
    ),
    tag = "scores"
)]
pub async fn add_score(
    State(state): State<AppState>,
    Json(payload): Json<models::GameResult>,
) -> Result<String, AppError> {
    let mut lock = state.db.lock().await;
    let puzzle_uid = payload.puzzle;
    let game_score: db_models::GameScore = payload.into();
    match lock.add_score(puzzle_uid, game_score) {
        Ok(Some(rank)) => Ok(format!("{rank}")),
        Ok(None) => {
            let msg = "New score not found in scores!";
            error!("{}", msg);
            Err(anyhow!(msg).into())
        }
        Err(e) => {
            error!("{}", e);
            Err(e.into())
        }
    }
}

/// Adds a new manifest and builds puzzles from all referenced images
#[utoipa::path(
    post,
    path = "/api/manifest",
    params(models::ManifestUrlQuery),
    security(
        ("api_key" = [])
    ),
    responses(
        (status = 202, description = "Request accepted", body = String),
        (status = 400, description = "Bad request"),
        (status = 500, description = "Internal server error")
    ),
    tag = "manifests"
)]
pub async fn add_manifest(
    Query(manifest_url): Query<models::ManifestUrlQuery>,
    State(state): State<AppState>,
) -> Response {
    let uid = cityhasher::hash::<u32>(&manifest_url.url);
    tokio::spawn(async move {
        match manifest_import::get_images_from_manifest(
            &manifest_url.url,
            state.db.clone(),
            state.http_client,
            &state.config.import_dir,
            false,
        )
        .await
        {
            Err(e) => {
                error!(
                    "Manifest import for url {} failed: {}",
                    &manifest_url.url, e
                );
            }
            Ok(None) => {}
            Ok(Some(manifest)) => {
                if let Err(e) = puzzle::create_puzzles_from_manifests(
                    &state.config,
                    state.db.clone(),
                    &[manifest],
                )
                .await
                {
                    error!(
                        "Creating puzzles from manifest {} failed: {}",
                        &manifest_url.url, e
                    )
                }
            }
        }
    });
    (StatusCode::ACCEPTED, uid.to_string()).into_response()
}

/// Deletes a manifest and related images/puzzles
#[utoipa::path(
    delete,
    path = "/api/manifest/{manifest_id}",
    security(
        ("api_key" = [])
    ),
    responses(
        (status = 200, description = "Manifest successfully deleted"),
        (status = 404, description = "No manifest with this id"),
        (status = 500, description = "Internal server error")
    ),
    tag = "manifests"
)]
pub async fn delete_manifest(
    Path(manifest_id): Path<u32>,
    State(state): State<AppState>,
) -> Result<(), AppError> {
    file_server::remove_puzzles_from_manifest(
        state.db.clone(),
        &state.config.puzzle_dir,
        manifest_id,
    )
    .await
    .map_err(|e| AppError::InternalError(e))?;
    let lock = state.db.lock().await;
    lock.delete_manifest(manifest_id)
        .map_err(|e| AppError::InternalError(e))?;
    let available_images = lock.get_all_available_images()?;
    file_server::image_index(available_images, &state.config.puzzle_dir)
        .map_err(|e| anyhow!(e).into())
}

/// Returns all manifests
#[utoipa::path(
    get,
    path = "/api/manifests", 
    responses(
        (status = 200, description = "List of manifest IDs", body = Vec<u32>),
        (status = 500, description = "Internal server error")
    ),
    tag = "manifests"
)]
pub async fn get_manifests(State(state): State<AppState>) -> Result<Json<Vec<u32>>, AppError> {
    let lock = state.db.lock().await;
    match lock.get_manifests() {
        Ok(i) => Ok(Json(i.iter().map(|i| i.uid).collect::<Vec<u32>>())),
        Err(e) => {
            error!("{}", e);
            Err(anyhow!(e).into())
        }
    }
}

/// Returns a manifest
#[utoipa::path(
    get,
    path = "/api/manifest/{manifest_id}",
    responses(
        (status = 200, description = "Manifest", body = Manifest), 
        (status = 404, description = "No manifest with this id")
    ),
    tag = "manifests"
)]
pub async fn get_manifest(
    Path(manifest_id): Path<u32>,
    State(state): State<AppState>,
) -> Result<Json<models::Manifest>, AppError> {
    let lock = state.db.lock().await;
    match lock.get_manifest(manifest_id) {
        Ok(Some(m)) => Ok(Json(m.into())),
        Ok(None) => {
            let err_msg = format!("No manifest with UID {} available", manifest_id);
            info!("{}", &err_msg);
            Err(AppError::NotFound(err_msg))
        }
        Err(e) => {
            error!("{}", e);
            Err(anyhow!(e).into())
        }
    }
}

/// Returns all image UIDs
#[utoipa::path(
    get,
    path = "/api/images", 
    responses(
        (status = 200, description = "List of image UIDs", body = Vec<u32>),
        (status = 500, description = "Internal server error")
    ),
    tag = "images"
)]
pub async fn get_images(State(state): State<AppState>) -> Result<Json<Vec<u32>>, AppError> {
    let lock = state.db.lock().await;
    match lock.get_all_available_images() {
        Ok(i) => Ok(Json(i.iter().map(|i| i.uid).collect::<Vec<u32>>())),
        Err(e) => {
            error!("{}", e);
            Err(anyhow!(e).into())
        }
    }
}

/// Returns information on a particular image
#[utoipa::path(
    get,
    path = "/api/image/{image_id}", 
    responses(
        (status = 200, description = "Information on image", body = ImageImport),
        (status = 404, description = "Image was not found"),
        (status = 500, description = "Internal server error")
    ),
    tag = "images"
)]
pub async fn get_image(
    Path(image_id): Path<u32>,
    State(state): State<AppState>,
) -> Result<Json<models::Image>, AppError> {
    let lock = state.db.lock().await;
    match lock.get_image(image_id) {
        Ok(Some(i)) => Ok(Json(models::Image {
            uid: i.uid,
            title: i.title,
            images: i.resized_images.into_iter().map(|i| i.image_path).collect(),
            thumbnails: i.thumbnails.into_iter().map(|t| t.image_path).collect(),
        })),
        Ok(None) => Err(AppError::NotFound(format!("Image with id {image_id}"))),
        Err(e) => {
            error!("{}", e);
            Err(anyhow!(e).into())
        }
    }
}
