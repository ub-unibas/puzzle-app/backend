//! Contains the utils to fetch, parse and manage IIIF manifests and their referenced image files

pub mod v2 {
    use serde::{Deserialize, Serialize};
    use std::collections::HashMap;

    use super::v3;

    #[derive(Serialize, Deserialize, Clone, Debug)]
    #[serde(untagged)]
    /// Container for values which could be multilingual
    pub enum MayBeI18NString {
        /// A string, not internationalised
        String(String),
        /// Strings, not internationalised
        Strings(Vec<String>),
        /// Internationalised strings
        I18NStrings(Vec<I18NString>),
        /// Single internationalised string
        I18NString(I18NString),
    }

    impl From<HashMap<String, Vec<String>>> for MayBeI18NString {
        fn from(value: HashMap<String, Vec<String>>) -> Self {
            if value.len() == 1 && value.contains_key("none") {
                Self::String(value.get("none").unwrap().join("\n"))
            } else if value.len() == 1 {
                let single_element = value.into_iter().next().unwrap();
                Self::I18NString(I18NString {
                    value: single_element.1.join("\n"),
                    language: single_element.0,
                })
            } else {
                Self::I18NStrings(
                    value
                        .into_iter()
                        .map(|(k, v)| I18NString {
                            value: v.join(" "),
                            language: k,
                        })
                        .collect::<Vec<I18NString>>(),
                )
            }
        }
    }

    impl From<MayBeI18NString> for HashMap<String, Vec<String>> {
        fn from(val: MayBeI18NString) -> Self {
            match val {
                MayBeI18NString::String(v) => Self::from([(
                    "none".to_string(),
                    v.split('\n')
                        .map(std::string::ToString::to_string)
                        .collect(),
                )]),
                MayBeI18NString::Strings(v) => Self::from([("none".to_string(), v)]),
                MayBeI18NString::I18NStrings(v) => v
                    .into_iter()
                    .map(|e| {
                        (
                            e.language,
                            e.value
                                .split('\n')
                                .map(std::string::ToString::to_string)
                                .collect(),
                        )
                    })
                    .collect(),
                MayBeI18NString::I18NString(v) => Self::from([(
                    v.language,
                    v.value
                        .split('\n')
                        .map(std::string::ToString::to_string)
                        .collect(),
                )]),
            }
        }
    }

    #[derive(Serialize, Deserialize, Clone, Debug)]
    pub struct I18NString {
        #[serde(rename = "@value")]
        value: String,
        #[serde(rename = "@language")]
        language: String,
    }

    #[derive(Serialize, Clone, Deserialize, Debug)]
    pub struct Manifest {
        pub label: MayBeI18NString,
        pub metadata: Option<Vec<StructuredMetadata>>,
        pub description: Option<MayBeI18NString>,
        pub attribution: Option<String>,
        pub license: Option<String>,
        //pub logo: Option<Logo>,
        /// Link to related external resource
        //pub related: Option<String>,
        pub sequences: Vec<Sequence>,
    }

    #[derive(Serialize, Clone, Deserialize, Debug)]
    pub struct Sequence {
        // TODO: attribution (for later)
        // TODO: license (for later)
        // TODO: logo (for later)
        pub canvases: Vec<Canvas>,
    }

    #[derive(Serialize, Clone, Deserialize, Debug)]
    pub struct Canvas {
        // TODO: attribution (for later)
        // TODO: license (for later)
        // TODO: logo (for later)
        pub images: Vec<Annotation>,
    }

    #[derive(Serialize, Clone, Deserialize, Debug)]
    pub struct Annotation {
        // TODO: attribution (for later)
        // TODO: license (for later)
        // TODO: logo (for later)
        pub resource: Resource,
    }

    #[derive(Serialize, Clone, Deserialize, Debug)]
    #[serde(untagged)]
    pub enum Resource {
        // Must be in this order, as Content is a subset of SpecificResource and would intercept
        // SpecificResources otherwise!
        SpecificResource(SpecificResource),
        Content(Content),
    }

    impl Resource {
        pub fn uri(&self) -> &str {
            match self {
                Self::SpecificResource(sr) => &sr.full.uri,
                Self::Content(c) => &c.uri,
            }
        }
    }

    #[derive(Serialize, Clone, Deserialize, Debug)]
    pub struct Content {
        // TODO: attribution (for later)
        // TODO: license (for later)
        // TODO: logo (for later)
        #[serde(rename = "@id")]
        pub uri: String,
        pub format: Option<String>,
        #[serde(rename = "@type")]
        pub resource_type: String,
    }

    #[derive(Serialize, Clone, Deserialize, Debug)]
    pub struct SpecificResource {
        #[serde(rename = "@id")]
        pub id: String,
        #[serde(rename = "@type")]
        pub resource_type: String,
        pub full: Content,
        pub selector: ImageApiSelector,
    }

    #[derive(Serialize, Deserialize, Clone, Debug)]
    pub struct StructuredMetadata {
        pub label: String,
        pub value: MayBeI18NString,
    }

    #[derive(Serialize, Deserialize, Clone, Debug)]
    pub struct Logo {
        #[serde(rename = "@id")]
        pub id: String,
    }

    impl From<StructuredMetadata> for super::v3::StructuredMetadata {
        fn from(val: StructuredMetadata) -> Self {
            Self {
                value: {
                    let h: HashMap<String, Vec<String>> = val.value.into();
                    h.into()
                },
                label: v3::MayBeI18NString::String(val.label),
            }
        }
    }

    #[derive(Serialize, Deserialize, Clone, Debug)]
    pub struct ImageApiSelector {
        #[serde(rename = "@type")]
        pub selector_type: String,
        pub region: Option<String>,
        pub size: Option<String>,
        pub rotation: Option<String>,
        pub quality: Option<String>,
        pub format: Option<String>,
    }

    impl From<ImageApiSelector> for super::v3::ImageApiSelector {
        fn from(val: ImageApiSelector) -> Self {
            Self {
                selector_type: val.selector_type,
                region: val.region,
                size: val.size,
                rotation: val.rotation,
                quality: val.quality,
                format: val.format,
            }
        }
    }
}

pub mod v3 {
    use serde::{Deserialize, Serialize};
    use std::collections::HashMap;
    use utoipa::ToSchema;

    /// Container for values which could be multilingual
    #[derive(Deserialize, Serialize, Clone, Debug, ToSchema)]
    #[serde(untagged)]
    pub enum MayBeI18NString {
        /// A string, not internationalised
        String(String),
        /// A internationalised string
        I18NString(HashMap<String, Vec<String>>),
    }

    impl From<HashMap<String, Vec<String>>> for MayBeI18NString {
        fn from(value: HashMap<String, Vec<String>>) -> Self {
            if value.len() == 1 && value.contains_key("none") {
                Self::String(value.get("none").unwrap().join("\n"))
            } else {
                Self::I18NString(value)
            }
        }
    }

    impl From<MayBeI18NString> for HashMap<String, Vec<String>> {
        fn from(val: MayBeI18NString) -> Self {
            match val {
                MayBeI18NString::String(v) => Self::from([(
                    "none".to_string(),
                    v.split('\n')
                        .map(std::string::ToString::to_string)
                        .collect(),
                )]),
                MayBeI18NString::I18NString(v) => v,
            }
        }
    }

    #[derive(Serialize, Deserialize, Debug, Clone)]
    #[serde(rename = "camelCase")]
    pub struct Manifest {
        pub label: MayBeI18NString,
        pub metadata: Option<Vec<StructuredMetadata>>,
        pub required_statement: Option<StructuredMetadata>,
        pub summary: Option<MayBeI18NString>,
        pub rights: Option<String>,
        /// Resource owner
        pub provider: Option<Vec<Agent>>,
        /// Web page that is about the object represented by the resource
        pub homepage: Option<Vec<Homepage>>,
        pub see_also: Option<SeeAlso>,
        pub items: Vec<Canvas>,
    }

    #[derive(Clone, Serialize, Deserialize, Debug)]
    pub struct Canvas {
        // TODO: metadata (for later)
        // TODO: requiredStatement (for later)
        // TODO: rights (for later)
        // TODO: homepage (for later)
        pub items: Vec<AnnotationPage>,
    }

    #[derive(Clone, Serialize, Deserialize, Debug)]
    pub struct AnnotationPage {
        // TODO: metadata (for later)
        // TODO: requiredStatement (for later)
        // TODO: rights (for later)
        pub items: Vec<Annotation>,
    }

    #[derive(Clone, Serialize, Deserialize, Debug)]
    pub struct Annotation {
        // TODO: metadata (for later)
        // TODO: requiredStatement (for later)
        // TODO: rights (for later)
        pub body: Resource,
    }

    #[derive(Clone, Serialize, Deserialize, Debug)]
    #[serde(untagged)]
    pub enum Resource {
        // Must be in this order, as Content is a subset of SpecificResource and would intercept
        // SpecificResources otherwise!
        SpecificResource(SpecificResource),
        Content(Content),
    }

    impl Resource {
        pub fn uri(&self) -> &str {
            match self {
                Self::SpecificResource(sr) => &sr.source.uri,
                Self::Content(c) => &c.uri,
            }
        }
    }

    #[derive(Clone, Serialize, Deserialize, Debug)]
    pub struct Content {
        // TODO: metadata (for later)
        // TODO: requiredStatement (for later)
        // TODO: rights (for later)
        #[serde(rename = "id")]
        pub uri: String,
        #[serde(rename = "type")]
        pub content_type: String,
        /// MIME format
        pub format: Option<String>,
    }

    #[derive(Clone, Serialize, Deserialize, Debug)]
    pub struct SpecificResource {
        pub id: String,
        #[serde(rename = "type")]
        pub resource_type: String,
        pub source: Content,
        pub selector: ImageApiSelector,
    }

    /// A structured, multilingual metadata field
    #[derive(Deserialize, Serialize, Clone, Debug, ToSchema)]
    pub struct StructuredMetadata {
        /// A (potentially multilingual) value
        pub value: MayBeI18NString,
        /// A (potentially multilingual) label
        pub label: MayBeI18NString,
    }

    #[derive(Deserialize, Serialize, Clone, Debug, ToSchema)]
    #[serde(rename_all = "camelCase")]
    pub struct Agent {
        pub id: String,
        #[serde(rename = "type")]
        pub provider_type: String,
        pub label: MayBeI18NString,
        #[serde(skip_serializing_if = "Option::is_none")]
        pub homepage: Option<Vec<Homepage>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        pub logo: Option<Vec<Logo>>,
        #[serde(skip_serializing_if = "Option::is_none")]
        pub see_also: Option<Vec<SeeAlso>>,
    }

    #[derive(Deserialize, Serialize, Clone, Debug, ToSchema)]
    pub struct Homepage {
        pub id: String,
        #[serde(rename = "type")]
        pub content_type: String,
        pub label: MayBeI18NString,
        #[serde(skip_serializing_if = "Option::is_none")]
        pub format: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        pub language: Option<Vec<String>>,
    }

    #[derive(Deserialize, Serialize, Clone, Debug, ToSchema)]
    pub struct Logo {
        pub id: String,
        #[serde(rename = "type")]
        pub content_type: String,
        #[serde(skip_serializing_if = "Option::is_none")]
        pub format: Option<String>,
    }

    #[derive(Deserialize, Serialize, Clone, Debug, ToSchema)]
    pub struct SeeAlso {
        pub id: String,
        #[serde(rename = "type")]
        pub content_type: String,
        #[serde(skip_serializing_if = "Option::is_none")]
        pub label: Option<MayBeI18NString>,
        #[serde(skip_serializing_if = "Option::is_none")]
        pub format: Option<String>,
        #[serde(skip_serializing_if = "Option::is_none")]
        pub profile: Option<String>,
    }

    #[derive(Serialize, Deserialize, Clone, Debug)]
    pub struct ImageApiSelector {
        #[serde(rename = "type")]
        pub selector_type: String,
        pub region: Option<String>,
        pub size: Option<String>,
        pub rotation: Option<String>,
        pub quality: Option<String>,
        pub format: Option<String>,
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::{fs::File, io::BufReader};

    #[test]
    fn manifest_v2() {
        let file = File::open("fixtures/manifest_v2.json").unwrap();
        let buf_reader = BufReader::new(file);
        let manifest: Result<v2::Manifest, serde_json::Error> = serde_json::from_reader(buf_reader);
        assert!(manifest.is_ok());
    }

    #[test]
    fn manifest_v3() {
        let file = File::open("fixtures/manifest_v3.json").unwrap();
        let buf_reader = BufReader::new(file);
        let manifest: Result<v3::Manifest, serde_json::Error> = serde_json::from_reader(buf_reader);
        assert!(manifest.is_ok());
    }

    #[test]
    fn manifest_v3_w_selector() {
        let file = File::open("fixtures/manifest_v3_w_selector.json").unwrap();
        let buf_reader = BufReader::new(file);
        let manifest: Result<v3::Manifest, serde_json::Error> = serde_json::from_reader(buf_reader);
        assert!(manifest.is_ok());
    }

    #[test]
    fn harvard() {
        let file = File::open("fixtures/harvard.json").unwrap();
        let buf_reader = BufReader::new(file);
        let manifest: Result<v2::Manifest, serde_json::Error> = serde_json::from_reader(buf_reader);
        assert!(manifest.is_ok());
    }
}
