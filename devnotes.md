# Notes for development

## Notes on ImageMagick

`+clone`: Inside parenthesis (where the operator is normally used) it will make a clone of the images from the last 'pushed' image sequence, and adds them to the end of the current image sequence. The +clone will simply make a copy of the last image in the image sequence.
`-fill Black`: Color to use when filling a graphic primitive.
`-colorize 100`: Colorize the image by an amount specified by value using the color specified by the most recent -fill setting.
`-fill White`
`-draw`: Annotate an image with one or more graphic primitives.
`-alpha off`: Gives control of the alpha/matte channel of an image.
`-compose CopyOpacity`: 
`-composite`: Perform alpha composition on two images and an optional mask
`-trim`: Trim an image. This option removes any edges that are exactly the same color as the corner pixels. 
`+repage`: Adjust the canvas and offset information of the image. Use +repage to completely remove/reset the virtual canvas meta-data from the images.
`+clone`
`-channel A`: Specify those image color channels to which subsequent operators are limited.
`-separate`
`+channel`
`-negate`
`-background black`
`-virtual-pixel background`
`-blur 0x2`
`-shade 120x21.78`
`-contrast-stretch 0%`
`+sigmoidal-contrast 7x50%`
`-fill grey50`
`-colorize 10%`
`+clone`
`+swap`
`-compose overlay`
`-composite`
`-compose In`
`-composite`

Maybe use a reduced processing pipeline for generating the jigsaw puzzle pieces:

```sh
{{input_file}} ( +clone -fill Black -colorize 100 -fill White -draw {{svg_path}} ) -alpha off -compose CopyOpacity -composite -trim +repage ( +clone -channel A -separate +channel -negate -background black -virtual-pixel background -blur 0x2 -shade 120x21.78 -contrast-stretch 0% +sigmoidal-contrast 7x50%  -fill grey50 -colorize 10% +clone +swap -compose overlay -composite ) -compose In -composite {{output_file}}
```
