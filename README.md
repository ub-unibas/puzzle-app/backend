# Puzzle App: Backend

The Puzzle App is a web based application for playing jigsaw puzzles. Its
hallmark feature is the support of [IIIF manifests](https://iiif.io/api/presentation/3.0/) as a way of importing new
images and their respective metadata.

This component (the backend) provides the server which generates the puzzles
and provides them to the
[frontend](https://gitlab.switch.ch/ub-unibas/puzzle-app/frontend). The SVG
paths used for the jigsaw templates are provided by the [puzzle-paths library](https://gitlab.switch.ch/ub-unibas/puzzle-app/puzzle-paths),
 which is part of this project.


## Requirements

- [ImageMagick](https://www.imagemagick.org) version >= 7.0
- A fairly current [Rust toolchain](https://rustup.rs) to compile the sources


## Caveats

The software is in its early stages and tested on Linux systems only!


## Setup

```sh
# In root dir
cargo build --release
```

After successfully building the application, you can find the binary in `target/release/image_server`.


## Importing manifests

### Manifests

Manifests are JSON files which provide a link to at least one image and contain related metadata. Three formats are supported by the application:

- A Manifest as defined in the [IIIF Presentation API version 2.1](https://iiif.io/api/presentation/2.1)
- A Manifest as defined in the [IIIF Presentation API version 3.0](https://iiif.io/api/presentation/3.0)
- A app-specific custom manifest format (see below)

#### Application-specific manifest format

The custom manifest has the aim to facilitate the import of images where no IIIF manifest yet exists. In its simplest form, it contains only a link to an image and a title:

```json
{
    "uris": ["https://example.com/test.jpg"],
    "label": "Some title"
}
```

As the type implies, the `uris` field can reference multiple images.

There are several optional fields as well:

```json
{
    "uris": ["https://example.com/test.jpg"],
    "label": "Some title",
    "summary": "A longer description",
    "rights": "A rights statement",
    "provider": ["https://example.com/provider"],
    "homepage": ["https://example.com", "https://example2.com"],
    "seeAlso": "https://example.com/image-dataset",
    "region": "0,0,300,500",
    "format": "image/jpeg",
    "metadata": {
        "location": "somewhere"
    }
}
```

The `metdata` field is a catch-all object: If you want to provide additional metadata, put it in here.

`label` and `summary` can be localised. Instead of e.g. writing `"label": "Some title"`, use an object with language codes as field labels and the localised strings as values:

```json
{
    "label": {
        "en": ["Some title"],
        "de": ["Ein Titel"]
    }
}
```

The `metadata` fields support localisation as well, but with a slightly different syntax:

```json
{
    "metadata": [
        {
            "label": {
                "en": ["location"],
                "de": ["Ort"]
            },
            "value": {
                "en": ["somewhere"],
                "de": ["irgendwo"]
            }
        }
    ]
}
```

It is even possible to combine the simple and the localised variant. E.g.:

```json
{
    "metadata": [
        {
            "label": "location",
            "value": {
                "en": ["somewhere"],
                "de": ["irgendwo"]
            }
        }
    ]
}
```

Finally, there are more expressive forms to describe a `homepage` or a `seeAlso` resource. Check out the [JSON schema](./manifest.schema.json) to learn more.


## Running the application

```sh
./puzzle_server <OPTIONS>
```

Depending on the amount of puzzles which have to be generated, it can take a
while until the web server is ready. It is recommended to set the environment
variable `RUST_LOG` to `info` in order to see when the puzzle creation has
finished and the web server is available.


### Configuration

Settings can be provided via command line arguments, environment variables or a
configuration file, the formers taking precedence over the latters. If no path
to a configuration file is define via command line arguments (`-c` /
`--config-file`) or environment variables (`CONFIG_FILE`), the configuration
file is expected to be found at `./configs/main.toml`. If a setting is not
defined by either of the three possible variants, a default value is used. See
these default values along with information on each setting by running
`./puzzle_server -h`. If you prefer to configure the application by file, you
can find a template in
[`puzzle_server/configs/main.example.toml`](./puzzle_server/configs/main.example.toml).


## Usage

### Puzzle server's file structure

While starting up, the server builds the puzzles in the `puzzles` subdirectory
and subsequently exposes this directory via the webserver on `<host>/puzzles/`.
The folder structure is as follows:

```sh
.
├── 2006011577                     # An image subdirectory
│   ├── 3736787576.jpg             # A resized copy of the original image
│   ├── 7x3                        # A puzzle from the image with a certain resolution
│   │   ├── 3736787576             # Folder containing the puzzle's pieces
│   │   │   ├── 0_0.png            # A puzzle piece
│   │   │   ├── <other_pieces>          
│   │   │   └── index.json         # Information on the puzzle's pieces
│   │   └── <other_geometry>
│   ├── index.json                 # Information on the image's puzzles
│   ├── thumbnail_2039956022.jpg   # A thumbnail of the image with a certain resolution
│   ├── <other_puzzle>
│   │   └── <...>
├── <other_image>
└── index.json                     # Information on available images
```

#### index.json

The `index.json` files found in the folder structures are intended to be consumed by a frontend application and provides the information needed to retrieve the puzzles. There are index files on three levels:

- The top-level `index.json` (`puzzles/index.json`) provides information on all
  images. It can e.g. be used to create an overview page.
- The `index.json` in an image folder (`puzzles/<image_id>/index.json`)
  contains links to all puzzles of the image as well as metadata on the image.
- The `index.json` in the pieces's folder
  (`puzzles/<image_id>/<puzzle>/<geometry_id>/index.json`) comprises the links
to all pieces of the puzzle. Furthermore, it contains the same metadata as the
image's `index.json`.


## Acknowledgements

- The jigsaw path library is heavily inspired by
  [@Draradech](https://github.com/Draradech)'s [Jigsaw Puzzle Generator](https://github.com/Draradech/jigsaw) 
(test it [here](https://draradech.github.io/jigsaw/jigsaw.html) in action).
- The ImageMagick arguments used to clip the jigsaw pieces are basically
  copied and pasted from a recipe on [ImageMagick's website](https://www.imagemagick.org/Usage/advanced/#jigsaw).


## Further resources

- [Code documentation](https://ub-unibas.pages.switch.ch/puzzle-app/backend/puzzle_server/).
- [Explanation on SVG path drawing capabilities in ImageMagick](https://imagemagick.org/Usage/draw/#paths)
- [Syntax of SVG's cubic Bézier curves](https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/d#cubic_b%C3%A9zier_curve)
