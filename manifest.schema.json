{
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "$id": "https://gitlab.switch.ch/ub-unibas/puzzle-app/backend/manifest.schema.json",
    "title": "Custom manifest",
    "description": "Describes an image to be imported by the app. Provides one or more links to images, and related metadata",
    "type": "object",
    "properties": {
        "uris": {
            "description": "One or more URIs linking to the images to be imported",
            "type": "array",
            "items": {
                "type": "string"
            },
            "minItems": 1,
            "uniqueItems": true
        },
        "label": {
            "description": "A short description of the image(s) equivalent to a title. A string or an object with the language code as field label and an array of localised strings as value",
            "type": ["string", "object"]
        },
        "summary": {
            "description": "A longer description of the image(s). A string or an object with the language code as field label and an array of localised strings as value",
            "type": ["string", "object"]
        },
        "rights": {
            "description": "Rights statement",
            "type": "string"
        },
        "provider": {
            "description": "List of resource owners. Items are strings or objects with the language code as field label and an array of localised strings as value",
            "type": "array",
            "items": {
                "type": ["string", "object"]
            }
        },
        "homepage": {
            "description": "List of related websites.",
            "type": "array",
            "items": {
                "type": ["string", "object"],
                "properties": {
                    "id": {
                        "description": "URI pointing to the resource",
                        "type": "string"
                    },
                    "type": {
                        "description": "Type of the website. Is normally `Text`.",
                        "type": "string"
                    },
                    "label": {
                        "description": "Title of the website. A string or an object with the language code as field label and an array of localised strings as value",
                        "type": ["string", "object"]
                    },
                    "format": {
                        "description": "MIME type of website",
                        "type": "string"
                    },
                    "language": {
                        "description": "Language codes for languages provided by the website",
                        "type": "array",
                        "items": {
                            "description": "Single language code, e.g. `en`",
                            "type": "string"
                        }
                    }
                },
                "required": ["id", "content_type", "label"]
            }
        },
        "seeAlso": {
            "description": "Related resource. Either a single URI or a URI object",
            "type": ["string", "object"],
            "properties": {
                "id": {
                    "description": "URI pointing to the resource",
                    "type": "string"
                },
                "type": {
                    "description": "Type of the website. E.g. `Dataset` or `Text`",
                    "type": "string"
                },
                "label": {
                    "description": "Title of the resource. A string or an object with the language code as field label and an array of localised strings as value",
                    "type": ["string", "object"]
                },
                "format": {
                    "description": "MIME type of the resource",
                    "type": "string"
                },
                "profile": {
                    "description": "A schema or named set of functionality available from the resource",
                    "type": "string"
                }
            },
            "required": ["id", "type"]
        },
        "region": {
            "description": "A subregion of the image which should be used for the puzzle. See https://iiif.io/api/image/3.0/#41-region for possible values.",
            "type": "string"
        },
        "rotation": {
            "description": "Rotation applied to the image. See https://iiif.io/api/image/3.0/#43-rotation for possible values",
            "type": "string"
        },
        "format": {
            "description": "MIME type of the image",
            "type": "string"
        },
        "metadata": {
            "description": "Additional metadata fields. Can have simple form, key-value-pairs, or a more advanced supporting localised strings in labels and values",
            "type": ["array", "object"],
            "items": {
                "description": "",
                "type": "object",
                "properties": {
                    "label": {
                        "description": "Field label. A string or an object with the language code as field label and an array of localised strings as value",
                        "type": ["string", "object"]
                    },
                    "value": {
                        "description": "Field value. A string or an object with the language code as field label and an array of localised strings as value",
                        "type": ["string", "object"]
                    }
                },
                "required": ["label", "value"]
            }
        }
    },
    "required": ["uris", "label"]
}
